﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Pause
{
    class PauseText : Text
    {
        public PauseText(Scene scene) : base(scene) { }

        private bool _canStart; // needed because of a bug; directly after activating, the startbutton is still sent to this object, and is set to true!

        public override void Load()
        {
            base.Load();

            Value = "-= Druk op Start/Enter om door te gaan =-";

            OnUpdate += TextPause_OnUpdate;
            OnKeyPress += PauseText_OnKeyPress;
            OnActivate += TextPause_OnActivate;
            OnGamepadPress += TextPause_OnGamepadPress;
        }

        private void PauseText_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Enter)) { GameMaster.SceneController.ActivateSceneSolo("main"); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                if (!_canStart)
                {
                    _canStart = true;
                }
                else
                {
                    GameMaster.SceneController.Unpause("main");
                    GameMaster.SceneController.ActivateSceneSolo("menu");
                }
            }
        }

        private void TextPause_OnUpdate(object sender, MonogameEngine.UpdateEventArgs e)
        {
            if (Position.Y > PositionStart.Y) { Jump(); }
        }

        private void Jump()
        {
            DirectionDegrees = 90;
            Speed = 3;
            Gravity = 0.2f;
        }


        private void TextPause_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start))
            {
                if (!_canStart)
                {
                    _canStart = true;
                }
                else
                {
                    GameMaster.SceneController.ActivateSceneSolo("main");
                }
            }
        }

        private void TextPause_OnActivate(object sender, EventArgs e)
        {
            CenterScreen();
            Position += new Vector2(0, 250);
            PositionStart = Position;

            Position -= new Vector2(0, 50);
            DirectionDegrees = 90;
            Gravity = 0.2f;
            Speed = 0;

            _canStart = false;
        }
    }
}

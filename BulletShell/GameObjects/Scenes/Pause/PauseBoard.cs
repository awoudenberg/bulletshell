﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Pause
{
    class PauseBoard : GameObject
    {
        public PauseBoard(Scene scene) : base(scene) { }

        #region members

        #endregion

        #region methods
        public override void Load()
        {
            Sprite = new Sprite(GameMaster.ResourceController.LoadResource<Texture2D>("Sprites/Pauze"));
            Sprite.DrawDepth = DrawDepths.Front;

            OnActivate += PauseBoard_OnActivate;
            OnUpdate += PauseBoard_OnUpdate;
        }

        private void PauseBoard_OnActivate(object sender, EventArgs e)
        {
            CenterScreen();
            Position -= new Vector2(0, 100);
            PositionStart = Position;

            Jump();
        }

        private void PauseBoard_OnUpdate(object sender, UpdateEventArgs e)
        {
            if (Position.Y > PositionStart.Y) { Jump(); }
        }

        private void Jump()
        {
            DirectionDegrees = 90;
            Speed = 1;
            Gravity = 0.01f;
        }
        #endregion
    }
}

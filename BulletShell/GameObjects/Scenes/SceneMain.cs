﻿using Master.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using BulletShell.GameObjects.Enemies;
using MonogameEngine;
using BulletShell.GameObjects.Scenes.Main;

namespace BulletShell.GameObjects.Scenes
{
    class SceneMain : Scene
    {
        MainSubmarine _submarine;
        MainOptions _options;

        public MainBackgroundManager BackgroundManager { get; set; }
        public MainEnemyController EnemyController { get; set; }
        public MainLevelManager LevelManager { get; set; }
        public MainLifeManager LifeManager { get; set; }

        public SceneMain(GameMaster game) : base(game)
        {
            Size = new Size(1280, 720);

            _submarine = new MainSubmarine(this);
            _options = new MainOptions(this);

            BackgroundManager = new MainBackgroundManager(this);
            EnemyController = new MainEnemyController(this);
            LevelManager = new MainLevelManager(this);
            LifeManager = new MainLifeManager(this);

            GameMaster.ScoreController.SetScene(this); // Make the scorecontroller display its contents on this scene.
        }

        internal void Reset()
        {
            if (!IsPaused)
            {
                _submarine.CenterScreen();
                _submarine.FreeMovement = GameMaster.SettingController.Settings.FreeMovement;
                _submarine.Position = new Vector2(150.0f, _submarine.Position.Y);

                DestroyResouceHoggers();
                LifeManager.ResetLives();
                LevelManager.Reset();
                GameMaster.ScoreController.Reset();
            }

            IsPaused = false;
        }


        internal void DestroyResouceHoggers()
        {
            var objects = new List<GameObject>();
            objects.AddRange(GetAllOfType(typeof(Enemy)));
            objects.AddRange(GetAllOfType(typeof(Bullet)));
            objects.AddRange(GetAllOfType(typeof(Text)));
            objects.AddRange(GetAllOfType(typeof(MainBubble)));
            objects.AddRange(GetAllOfType(typeof(MinePiece)));
            objects.ForEach(o => o.ForceDestroy());
        }
    }
}

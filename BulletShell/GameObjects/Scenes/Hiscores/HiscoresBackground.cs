﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using MonogameEngine.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Hiscores
{
    class HiscoresBackground : GameObject
    {
        public HiscoresBackground(Scene scene) : base(scene) { }

        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Background"));
            Sprite.DrawDepth = DrawDepths.Back;
        }
    }
}

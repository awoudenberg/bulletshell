﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Hiscores
{
    internal class HiscoresDisplayLine : GameObject
    {
        public Hiscore Hiscore
        {
            get { return _hiscore; }
            set
            {
                _hiscore = value;
                UpdateDisplayString();
            }
        }


        public bool IsNew
        {
            get { return _isNew; }
            set
            {
                _isNew = value;

                if (value) { _colorTimer.Start(); }
            }
        }

        public static string Font {
            get { return _font; }
        }

        private const string _font = "hiscores";


        private Hiscore _hiscore;
        private string _displayString;
        private Timer _colorTimer;
        private bool _isNew;
        private int _colorIndex;

        private Color[] _colors = { Color.Red, Color.Purple, Color.Blue, Color.Green, Color.Yellow, Color.Orange };

        public HiscoresDisplayLine(Scene scene) : base(scene) { }

        public override void Load()
        {
            _hiscore = new Hiscore { Name = "", Score = 0, Date = DateTime.Now };
            _displayString = "";
            _colorIndex = 0;
            _isNew = false;

            _colorTimer = new Timer();
            _colorTimer.Interval = 100;
            _colorTimer.Elapsed += _colorTimer_Elapsed;

            Sprite.Color = Color.Red;
            Sprite.DrawDepth = DrawDepths.Front;

            OnUpdate += HiscoresDisplayLine_OnUpdate;
            OnAfterDraw += HiscoresDisplayLine_OnAfterDraw;
            OnDeactivate += HiscoresDisplayLine_OnDeactivate;
        }

        private void HiscoresDisplayLine_OnUpdate(object sender, UpdateEventArgs e)
        {
            if(Position.Y>=PositionTarget.Y) {
                Position = PositionTarget;
                PositionTarget = Position;
            }
        }

        private void HiscoresDisplayLine_OnAfterDraw(object sender, DrawEventArgs e)
        {
            GameMaster.TextController.Write(Position, _displayString, Sprite.Color, _font);
        }

        private void _colorTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _colorIndex = (_colorIndex + 1) % _colors.Length;
            Sprite.Color = _colors[_colorIndex];
        }

        

        private void HiscoresDisplayLine_OnDeactivate(object sender, EventArgs e)
        {
            Destroy();
        }


        private void UpdateDisplayString()
        {
            if (Hiscore != null)
            {
                var name = (Hiscore.Name.Length <= 18 ? Hiscore.Name : Hiscore.Name.Substring(0, 18));
                var score = (Hiscore.Score == 0 ? "0" : Hiscore.Score.ToString());

                _displayString = String.Format("{0,-18:#} {1,9:#}", name, score);

                Sprite.Size = GameMaster.TextController.CalculateSize(_displayString, _font);

                CenterScreen();
            }
        }


        
    }
}
﻿using Master.Engine;
using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Hiscores
{
    class HiscoresControl : GameObject
    {
        private List<HiscoresDisplayLine> _displayLines;
        private List<Hiscore> _hiscores;
        private int _hiscoreIndex;
        private bool _isListening;


        public HiscoresControl(Scene scene) : base(scene) { }

        public override void Load()
        {
            _displayLines = new List<HiscoresDisplayLine>();
            _isListening = false;

            OnActivate += HiscoresManager_OnActivate;
            OnKeyPress += HiscoresManager_OnKeyPress;
            OnGamepadPress += HiscoresManager_OnGamepadPress;
            OnGamepadRelease += HiscoresManager_OnGamepadRelease;
            OnKeyRelease += HiscoresManager_OnKeyRelease;
        }


        private void HiscoresManager_OnActivate(object sender, EventArgs e)
        {
            Utility.DestroyAll(_displayLines);

            _hiscoreIndex = (Scene.IsSolo ? -1 : GameMaster.ScoreController.AddHiscore());
            _hiscores = GameMaster.ScoreController.LoadHiscores();
            _isListening = false;

            (GameMaster.SceneController.GetScene("main") as SceneMain).DestroyResouceHoggers();
            Sprite.Size = new Vector2(0, _hiscores.Count * GameMaster.TextController.CalculateSize("TESTSTRING", HiscoresDisplayLine.Font).Y);
            CenterScreen();

            for (int i = 0; i < _hiscores.Count; i++)
            {
                var displayLine = new HiscoresDisplayLine(Scene);

                displayLine.Hiscore = _hiscores[i];
                displayLine.Position = new Vector2(displayLine.Position.X, -20);
                displayLine.PositionTarget = new Vector2(displayLine.Position.X, Position.Y + 20 * i);
                displayLine.Speed = 4;

                _displayLines.Add(displayLine);
            }

            if (_hiscoreIndex != -1) { _displayLines[_hiscoreIndex].IsNew = true; }

        }

        private void HiscoresManager_OnKeyRelease(object sender, MonogameEngine.Controllers.KeyboardReleasedEventArgs e)
        {
            _isListening = true;
        }

        private void HiscoresManager_OnGamepadRelease(object sender, MonogameEngine.Controllers.GamepadReleasedEventArgs e)
        {
            _isListening = true;
        }


        private void HiscoresManager_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (IsActive && _isListening && e.Keys.Contains(Keys.Escape)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
        }

        private void HiscoresManager_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (IsActive && _isListening && e.Buttons.Contains(Buttons.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
        }

    }
}

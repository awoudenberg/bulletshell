﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Hiscores
{
    class HiscoresText : Text
    {
        public HiscoresText(Scene scene) : base(scene) { }

        private bool _canStart; // needed because of a bug; directly after activating, the startbutton is still sent to this object, and is set to true!

        public override void Load()
        {
            base.Load();

            Value = "-= Druk op een Escape/Back om door te gaan =-";

            OnUpdate += TextPause_OnUpdate;
            OnActivate += TextPause_OnActivate;
        }
        

        private void TextPause_OnUpdate(object sender, MonogameEngine.UpdateEventArgs e)
        {
            if (Position.Y > PositionStart.Y) { Jump(); }
        }

        private void Jump()
        {
            DirectionDegrees = 90;
            Speed = 3;
            Gravity = 0.2f;
        }

        private void TextPause_OnActivate(object sender, EventArgs e)
        {
            CenterScreen();
            Position += new Vector2(0, 250);
            PositionStart = Position;

            Position -= new Vector2(0, 50);
            DirectionDegrees = 90;
            Gravity = 0.2f;
            Speed = 0;

            _canStart = false;
        }
    }
}

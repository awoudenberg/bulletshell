﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Hiscores
{
    internal class HiscoresFrame : GameObject
    {
        public HiscoresFrame(Scene scene) : base(scene) { }

        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Backgrounds/hiscore"));
            Sprite.DrawDepth = DrawDepths.Front - 0.0000000001f;

            OnActivate += HiscoresFrame_OnActivate;
        }

        private void HiscoresFrame_OnActivate(object sender, EventArgs e)
        {
            CenterScreen();
        }
    }
}
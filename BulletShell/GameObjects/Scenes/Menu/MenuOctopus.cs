﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Menu
{
    class MenuOctopus : GameObject
    {
        public MenuOctopus(Scene scene) : base(scene) { }

        #region members
        private const string _soundChangeSetting = "96125__bmaczero__bubbles-single2";
        private const string _spriteOctopus = "Sprites/SpriteOctopusMenu";
        private int _option;
        #endregion

        #region methods
        public override void Load()
        {
            _option = 0;

            Sprite = new Sprite(GameMaster.ResourceController.LoadResource<Texture2D>(_spriteOctopus), 6, 1, 0);
            Sprite.CurrentFrame = _option;
            Sprite.Origin = new Vector2(Sprite.Size.X, 0); // topright

            OnGamepadPress += MenuOctopus_OnGamepadPress;
            OnActivate += MenuOctopus_OnActivate;
            OnKeyPress += MenuOctopus_OnKeyPress;
        }

        private void MenuOctopus_OnActivate(object sender, EventArgs e)
        {
            GameMaster.SoundController.Loop("menu");
            Position = new Vector2(GameMaster.Size.Width, 180);
        }

        private void MenuOctopus_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape)) { GameMaster.Exit(); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.S) || e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Down)) { _option = (_option + 1) % 4; GameMaster.SoundController.Play(_soundChangeSetting); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.W) || e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Up)) { _option = (_option + 3) % 4; GameMaster.SoundController.Play(_soundChangeSetting); }

            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Space) || e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Enter)) { PickOption(); }

            Sprite.CurrentFrame = _option;
        }

        private void MenuOctopus_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.DPadDown)) { _option = (_option + 1) % 4; GameMaster.SoundController.Play(_soundChangeSetting); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.DPadUp)) { _option = (_option + 3) % 4; GameMaster.SoundController.Play(_soundChangeSetting); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start) || e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.B)) { PickOption(); }

            Sprite.CurrentFrame = _option;
        }

        private void PickOption()
        {
            if (_option == 0) { GameMaster.SceneController.ActivateSceneSolo("main"); }
            if (_option == 1) { GameMaster.SceneController.ActivateSceneSolo("settings"); }
            if (_option == 2) { GameMaster.SceneController.ActivateSceneSolo("hiscores"); }
            if (_option == 3) { GameMaster.Exit(); }
        }



        #endregion
     }
}

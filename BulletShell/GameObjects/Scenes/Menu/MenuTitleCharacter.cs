﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Menu
{
    class MenuTitleCharacter : GameObject
    {
        private float _waveSpeed;
        private float _waveOffset;
        private float _waveAmplitude;

        public MenuTitleCharacter(Scene scene) : base(scene) { }


        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("stripName"), 9, 2, 0);
            Sprite.Origin = new Vector2(0, 0);

            _waveAmplitude = 40;
            _waveSpeed = 0.05f;
            _waveOffset = 0;

            OnUpdate += MenuTitleCharacter_OnUpdate;
        }

        private void MenuTitleCharacter_OnUpdate(object sender, UpdateEventArgs e)
        {
            _waveOffset = (_waveOffset + _waveSpeed) % 360;

            Position = new Vector2(Position.X, PositionStart.Y + _waveAmplitude * (float)Math.Sin(_waveOffset));
        }

        public float WaveOffset
        {
            get { return _waveOffset; }
            set { _waveOffset = value; }
        }
    }
}

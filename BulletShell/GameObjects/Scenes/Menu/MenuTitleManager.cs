﻿using BulletShell.GameObjects.Enemies;
using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Menu
{
    class MenuTitleManager : GameObject
    {
        private List<MenuTitleCharacter> _characters = new List<MenuTitleCharacter>();

        public MenuTitleManager(Scene scene) : base(scene) { }

        public override void Load()
        {
            MenuTitleCharacter character;

            Position = new Vector2(80, 80);

            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    character = new MenuTitleCharacter(Scene);
                    character.Position = Position + new Vector2(0.5f * x * character.Sprite.Size.X, y * character.Sprite.Size.Y);
                    character.PositionStart = character.Position;
                    character.Sprite.CurrentFrame = 9 * y + x;
                    character.WaveOffset = x / 4.0f;

                    _characters.Add(character);
                }
            }
        }
    }
}

﻿using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes.Pause;
using Microsoft.Xna.Framework.Graphics;

namespace BulletShell.GameObjects.Scenes
{
    class ScenePause : Scene
    {
        public ScenePause(GameMaster game) : base(game)
        {
            var txtPause = new PauseText(this);
            var board = new PauseBoard(this);           
        }
    }
}

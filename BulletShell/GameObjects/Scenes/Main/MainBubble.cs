﻿using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using MonogameEngine.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainBubble : GameObject
    {
        private const float _speedMax = 3.0f;

        public MainBubble(Scene scene) : base(scene) { }

        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("bubble"));
            Sprite.Scale *= (float)Utility.GetRandom(3) / 10 + 0.2f;
            Gravity = -Sprite.Scale.X / 3; // Big bubbles rise quicker :)

            OnOutsideScreen += Bubble_OnOutsideScreen;
            OnUpdate += Bubble_OnUpdate;
        }

        private void Bubble_OnUpdate(object sender, UpdateEventArgs e)
        {
            Utility.Limit(Speed, -_speedMax, _speedMax);
        }

        private void Bubble_OnOutsideScreen(object sender, EventArgs e)
        {
            Destroy();
        }
    }
}

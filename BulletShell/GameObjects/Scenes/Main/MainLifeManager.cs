﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainLifeManager : GameObject
    {
        #region Constructor
        public MainLifeManager(Scene scene) : base(scene) { }
        #endregion

        #region Members
        private const string _soundGameOver = "178876__rocotilos__game-over-evil";

        public int Lives { get; set; }
        public List<MainLifeBubble> Bubbles { get; set; }
        #endregion

        #region Methods
        public override void Load()
        {
            Position = new Vector2(35, 75);
            ResetLives();
        }

        public void ResetLives()
        {
            if (Bubbles == null) { Bubbles = new List<MainLifeBubble>(); }

            Lives = 3;

            for (int i = Bubbles.Count; i < Lives; i++)
            {
                var bubble = new MainLifeBubble(Scene);

                bubble.Position = new Vector2(Position.X + bubble.Sprite.Size.X * 0.8f * i, Position.Y);

                Bubbles.Add(bubble);
            }
        }

        public void LoseLife()
        {
            if(!IsActive) { return; } // bugfix

            if (Lives == 0)
            {
                GameMaster.SoundController.Play(_soundGameOver);
                GameMaster.SceneController.ActivateScene("hiscores");
            }
            else
            {
                Bubbles.Last().Destroy();
                Bubbles.Remove(Bubbles.Last());
                Lives--;
            }


        }
        #endregion
    }
}

﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainLifeBubble : GameObject
    {
        public MainLifeBubble(Scene scene) : base(scene) { }

        public override void Load()
        {
            Sprite = new Sprite(GameMaster.ResourceController.LoadResource<Texture2D>("Sprites/Bubble7"));
            Sprite.DrawDepth = DrawDepths.Front;
        }
    }
}

﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    /// <summary>
    /// Testclass to display the fact that I can alter certain things within the engine, among which the volume, pitch and pan
    /// of the soundengine.
    /// </summary>
    class MainOptions : GameObject
    {
        public MainOptions(Scene scene) : base(scene) { }

        #region members
        private const float _settingStepSize = 0.01f;
        #endregion

        #region methods
        public override void Load()
        {
            OnKeyDown += MainOptions_OnKeyDown;
        }

        private void MainOptions_OnKeyDown(object sender, MonogameEngine.Controllers.KeyboardDownEventArgs e)
        {
            // Control the pitch of the soundengine.
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Q)) { GameMaster.SoundController.MasterPitch += _settingStepSize; }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.A)) { GameMaster.SoundController.MasterPitch = 0.0f; }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Z)) { GameMaster.SoundController.MasterPitch -= _settingStepSize; }

            // Control the pan of the soundeffects.
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.W)) { GameMaster.SoundController.MasterPan += _settingStepSize; }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.S)) { GameMaster.SoundController.MasterPan = 0.0f; }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.X)) { GameMaster.SoundController.MasterPan -= _settingStepSize; }

            // Control the volume of the soundeffects.
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.E)) { GameMaster.SoundController.MasterVolume += _settingStepSize; }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.C)) { GameMaster.SoundController.MasterVolume -= _settingStepSize; }
        }
        #endregion
    }
}

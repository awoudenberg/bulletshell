﻿using BulletShell.GameObjects.Bullets;
using BulletShell.GameObjects.Enemies;
using BulletShell.GameObjects.Scenes;
using BulletShell.GameObjects.Scenes.Main;
using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainSubmarine : GameObject
    {
        private const string _soundHit = "215162__otisjames__thud";
        private const string _soundLaser = "273497__thegeekranger__laser-gun-shot";
        private const string _songMain = "main";
        private const string _textureBoat = "Strip SubmarineSidewaysBarrel1";

        private const float _movementSpeedNormal = 8.0f; // Movementspeed without any powerups
        private const float _movementSpeedHyper = 15.0f; // Movementspeed with the powerup (TODO: currently by pressing LeftShoulderButton)
        private const float _maxAngle = 30.0f; // The maximum rotation allowed, in both directions
        private const int _bulletInterval = 100; // The amount of time before the player can shoot another bullet
        private const int _bubbleInterval = 150; // The amount of milliseconds before another bubble should be emitted from the tail
        private const float _rotationSpeed = 1.0f; // The maximum speed with which the boat rotates towards the desired angle

        private bool _canShoot = true; // Can another bullet be fired?
        private float _desiredRotation = 0.0f; // The targetangle we should try to get the boat in

        private Timer _timerBullet; // Timer to get the bullet fireable again
        private Timer _timerBubble; // timer to periodicly shoot bubbles from the tail of the boat
        private Timer _timerInvincible; // Timer to see how long we're invincible
        private int _invincibleDuration; // the amount of frames we should stay invincible
        private Vector2 _bulletOffset; // The position of the gunbarrel, as an offset from the origin of the sprite (center)
        private Vector2 _bubbleOffset; // The position of the tail of the boat, as an offset from the origin of the sprite (center)

        public bool IsInvincible { get; set; }
        public bool FreeMovement { get; set; } // Can the player move around the entire screen, or only vertical?

        public MainSubmarine(Scene scene) : base(scene)
        {
            IsInvincible = true; // Has to be here, to make sure the enemies don't kill it immediately.
        }


        // TODO: Mirror the image in photoshop.
        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>(_textureBoat), 6, 1);
            Sprite.SpriteEffects = SpriteEffects.FlipHorizontally;

            _timerBullet = new Timer(_bulletInterval);
            _timerBullet.Elapsed += TimerBullet_Elapsed;
            _timerBullet.Start();

            _timerBubble = new Timer(_bubbleInterval);
            _timerBubble.Elapsed += _timerBubble_Elapsed;
            _timerBubble.Start();

            _invincibleDuration = 360;
            _timerInvincible = new Timer(_invincibleDuration);
            _timerInvincible.Elapsed += _timerInvincible_Elapsed;
            _timerInvincible.Start();

            _bulletOffset = new Vector2(100, -4);
            _bubbleOffset = new Vector2(-122, 8);

            OnGamepadDown += Submarine_OnGamepadDown;
            OnGamepadPress += Submarine_OnGamepadPress;
            OnKeyDown += Submarine_OnKeyDown;
            OnKeyPress += Submarine_OnKeyPress;
            OnUpdate += Submarine_OnUpdate;
            OnActivate += Submarine_OnActivate;
        }

        private void Submarine_OnActivate(object sender, EventArgs e)
        {
            GameMaster.SoundController.Loop(_songMain);

            (Scene as SceneMain).Reset();
        }

        private void _timerInvincible_Elapsed(object sender, ElapsedEventArgs e)
        {
            Sprite.Color = Color.White;
            IsInvincible = false;
            _timerInvincible.Stop();
        }

        internal void HitByEnemy()
        {
            GameMaster.SoundController.Play(_soundHit, Utility.CalculatePan(Position));

            Sprite.Color = Color.Pink;
            IsInvincible = true;
            _timerInvincible.Start();
        }

        private void Submarine_OnKeyDown(object sender, MonogameEngine.Controllers.KeyboardDownEventArgs e)
        {
            if (!IsActive) { return; }

            Position += (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Up) ? _movementSpeedHyper : _movementSpeedNormal) * new Vector2(0, -1);
            Position += (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Down) ? _movementSpeedHyper : _movementSpeedNormal) * new Vector2(0, 1);
            Position += (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Left) && FreeMovement ? _movementSpeedHyper : _movementSpeedNormal) * new Vector2(-1, 0);
            Position += (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Right) && FreeMovement ? _movementSpeedHyper : _movementSpeedNormal) * new Vector2(1, 0);

            // Shooting
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Space) && _canShoot) { ShootBullet(); }
        }

        private void Submarine_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape)) { Scene.IsPaused = true; GameMaster.SceneController.ActivateScene("pause"); }
        }



        /// <summary>
        /// Spawn a bubble from the tail of the boat. It gets a bit of a boost in the opposite direction of the boat,
        /// as well as a random speed (throught the boat) and size (through the bubble itself).
        /// </summary>
        private void _timerBubble_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsActive)
            {
                for (int i = 0; i < GameMaster.SettingController.Settings.AmountOfBubblesBoat; i++)
                {
                    var bubble = new MainBubble(Scene);

                    bubble.Position = CalculateRotatedOffset(_bubbleOffset);
                    bubble.DirectionDegrees = Sprite.Rotation + 180.0f;
                    bubble.Speed = (float)Utility.GetRandom(3) / 5 + 1.0f;
                }
            }
        }

        /// <summary>
        /// Update the boat. If the rightthumbstick is not held, we (try to) level the boat. If the boat would have been
        /// outside the screen, we place it back, making sure the player cannot leave the screen. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Submarine_OnUpdate(object sender, UpdateEventArgs e)
        {
            if (e.Thumbsticks.Right.Length() == 0.0f) { _desiredRotation = 0.0f; }

            Sprite.Rotation += Utility.Limit(_desiredRotation - Sprite.Rotation, -_rotationSpeed, _rotationSpeed);

            PlaceWithinScreen();
        }

        /// <summary>
        /// Currently toggles the free movement of the boat.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Submarine_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start)) { Scene.IsPaused = true; GameMaster.SceneController.ActivateScene("pause"); }
        }

        /// <summary>
        /// Enable a bullet to be fired from the barrel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerBullet_Elapsed(object sender, ElapsedEventArgs e)
        {
            _canShoot = true;
        }

        /// <summary>
        /// Handle userinput, including movement and shooting.
        /// </summary>
        private void Submarine_OnGamepadDown(object sender, MonogameEngine.Controllers.GamepadDownEventArgs e)
        {
            if (!IsActive) { return; }

            Position += (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.LeftShoulder) ? _movementSpeedHyper : _movementSpeedNormal) * e.ThumbstickLeft;
            _desiredRotation = Utility.Limit((e.ThumbstickRight == new Vector2(0, 0) ? Sprite.Rotation : Utility.VectorToAngle(e.ThumbstickRight)), -_maxAngle, _maxAngle);

            if (!FreeMovement) { Position = new Vector2(PositionPrevious.X, Position.Y); }

            // Shooting
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.RightTrigger) && _canShoot) { ShootBullet(); }
        }

        private void ShootBullet()
        {
            var bullet = new PlayerBullet(Scene);

            bullet.Position = CalculateRotatedOffset(_bulletOffset);
            bullet.DirectionDegrees = -Sprite.Rotation;

            GameMaster.SoundController.Play(_soundLaser, Utility.CalculatePan(Position), 0.05f);

            _canShoot = false;
        }
    }



}

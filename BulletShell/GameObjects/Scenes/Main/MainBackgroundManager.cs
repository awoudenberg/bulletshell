﻿using BulletShell.GameObjects.Enemies;
using Master.Engine;
using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Main
{
    public class MainBackgroundManager : GameObject
    {
        public MainBackgroundManager(Scene scene) : base(scene) { }


        public float GlobalSpeed { get { return _globalSpeed; } }

        private List<MainBackground> _layers;
        private MainBackground _sea;
        private MainBackground _sand;
        private MainBackground _sandBG;
        private Timer _timerPlant;
        private float _globalSpeed;

        public override void Load()
        {
            _globalSpeed = 1.5f;

            _layers = new List<MainBackground>();

            // Sea layer
            _sea = new MainBackground(Scene);
            _sea.Sprite = new Sprite(Resources.LoadResource<Texture2D>("Backgrounds/BlueBackgroundwaves"));
            _sea.Sprite.DrawDepth = DrawDepths.Back;
            _sea.Sprite.Origin = new Vector2(0, _sea.Sprite.Size.Y);
            _sea.Speed = _globalSpeed * 0.4f;
            _sea.Position = new Vector2(0, Scene.Size.Height);
            _layers.Add(_sea);

            _sandBG = new MainBackground(Scene);
            _sandBG.Sprite = new Sprite(Resources.LoadResource<Texture2D>("Backgrounds/SandVague"));
            _sandBG.Sprite.DrawDepth = DrawDepths.Back - 0.002f;
            _sandBG.Sprite.Origin = new Vector2(0, _sandBG.Sprite.Size.Y - 100);
            _sandBG.Speed = _globalSpeed * 0.5f;
            _sandBG.Position = new Vector2(-Scene.Size.Width, Scene.Size.Height);
            _layers.Add(_sandBG);

            _sand = new MainBackground(Scene);
            _sand.Sprite = new Sprite(Resources.LoadResource<Texture2D>("Backgrounds/SandSmall"));
            _sand.Sprite.DrawDepth = DrawDepths.Back - 0.004f;
            _sand.Sprite.Origin = new Vector2(0, _sand.Sprite.Size.Y - 55);
            _sand.Speed = _globalSpeed * 0.7f;
            _sand.Position = new Vector2(0, Scene.Size.Height);
            _layers.Add(_sand);

            _timerPlant = new Timer();
            _timerPlant.Elapsed += _timerPlant_Elapsed;
            _timerPlant.Interval = 1500;
            _timerPlant.Start();
        }


        public void ChangeGlobalSpeed(float globalSpeed)
        {
            var objectsToChange = new List<GameObject>();
            var deltaSpeed = globalSpeed / _globalSpeed;
            var enemies = Scene.GetAllOfType(typeof(Enemy));
            var backdrops = Scene.GetAllOfType(typeof(MainBackdrop));

            objectsToChange.AddRange(enemies);
            objectsToChange.AddRange(backdrops);
            objectsToChange.AddRange(_layers);
            objectsToChange.ForEach(o => o.DirectionVector *= deltaSpeed);

            _globalSpeed = globalSpeed;
        }


        private void _timerPlant_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsActive) { new MainBackdrop(Scene); }
        }

    }
}

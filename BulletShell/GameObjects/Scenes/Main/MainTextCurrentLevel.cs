﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainTextCurrentLevel : Text
    {
        public MainTextCurrentLevel(Scene scene) : base(scene) { }

        private Timer _timerDestroy;

        public override void Load()
        {
            base.Load();

            _timerDestroy = new Timer();
            _timerDestroy.Interval = 3000;
            _timerDestroy.Elapsed += _timerDestroy_Elapsed;
            _timerDestroy.Start();
        }

        private void _timerDestroy_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(IsActive)
            {
                _timerDestroy?.Stop();
                Destroy();
            }
        }
    }
}

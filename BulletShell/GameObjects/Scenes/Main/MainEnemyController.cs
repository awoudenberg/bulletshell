﻿using BulletShell.GameObjects.Enemies;
using BulletShell.GameObjects.Scenes.Main;
using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Main
{
    public class MainEnemyController : GameObject
    {
        private Timer _timerSpawn;
        private Random _rng; // todo: use Utility.


        private List<string> _enemyNames;
        private int _intervalMin;
        private int _intervalMax;


        public MainEnemyController(Scene scene) : base(scene) { }

        public override void Load()
        {
            _rng = new Random();

            _timerSpawn = new Timer();
            _timerSpawn.Elapsed += _timerSpawn_Elapsed;

            OnActivate += MainEnemyController_OnActivate;
        }

        private void MainEnemyController_OnActivate(object sender, EventArgs e)
        {
            StartMakingEnemies();
        }

        private void _timerSpawn_Elapsed(object sender, ElapsedEventArgs e)
        {
            GameObject enemy;

            if (IsActive)
            {
                switch (_enemyNames[Utility.GetRandom(_enemyNames.Count)])
                {
                    case "Fish": enemy = new EnemyFish(Scene); break;
                    case "Anglerfish": enemy = new EnemyAnglerfish(Scene); break;
                    case "Starfish": enemy = new EnemyStarfish(Scene); break;

                    case "Mine":
                    default:
                        enemy = new EnemyMine(Scene); break;
                }
            }

            _timerSpawn.Interval = Utility.GetRandom(_intervalMin, _intervalMax);
        }

        public void StartMakingEnemies()
        {
            var level = (Scene as SceneMain).LevelManager.CurrentLevel;

            _intervalMin = level.EnemyIntervalInMilliSecondsMinimum;
            _intervalMax = level.EnemyIntervalInMilliSecondsMaximum;
            _enemyNames = level.EnemyNames;
            _timerSpawn.Interval = Utility.GetRandom(_intervalMin, _intervalMax);
            _timerSpawn.Start();
        }




        //private void TimerSpawn_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    GameObject enemy;
        //    float y;

        //    if (!IsActive) { return; }

        //    _timerSpawn.Interval = _rng.Next(_intervalMin, _intervalMax);
        //    y = 0;

        //    switch (_rng.Next(1)) // TODO: enkel mijnen nu
        //    {
        //        case 0: enemy = new EnemyMine(Scene); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //        case 1: enemy = new Shell(GameMaster); y = enemy.Position.Y; break;
        //        case 2: enemy = new Starfish(GameMaster); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //        case 3: enemy = new Anglerfish(GameMaster); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //        case 4: enemy = new EnemyFish(GameMaster); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //        case 5: enemy = new Jellyfish(GameMaster); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //        case 6: enemy = new Octopus(GameMaster); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;


        //        default: enemy = new EnemyMine(Scene); y = _rng.Next((int)enemy.Sprite.Size.Y, (int)(GameMaster.Size.Height - enemy.Sprite.Size.Y)); break;
        //    }

        //    enemy.Speed = (Scene as SceneMain).BackgroundManager.GlobalSpeed;
        //    enemy.DirectionDegrees = 180;
        //    enemy.Position = new Vector2(GameMaster.Size.Width + 100, y);
        //}

        internal void DestroyAll()
        {
            var enemies = Scene.GetAllOfType(typeof(Enemy));

            foreach (var enemy in enemies)
            {
                enemy.Destroy();
            }
        }
    }
}

﻿using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using MonogameEngine.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainBackground : GameObject
    {
        public MainBackground(Scene scene) : base(scene) { }




        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Background"));
            Sprite.DrawDepth = DrawDepths.Back;
            Sprite.Origin = new Vector2(0, 0);

            Position = new Vector2(0, 0);

            Speed = 1.5f;
            DirectionDegrees = 180;

            OnUpdate += Background_OnUpdate;
            OnAfterDraw += Background_OnAfterDraw;
        }


        private void Background_OnAfterDraw(object sender, DrawEventArgs e)
        {
            Sprite.Draw(e.SpriteBatch, Position + new Vector2(Sprite.Size.X, 0));
        }

        private void Background_OnUpdate(object sender, UpdateEventArgs e)
        {
            if (Position.X <= -Sprite.Size.X) { Position = new Vector2(0, GameMaster.Size.Height); }
        }


    }
}

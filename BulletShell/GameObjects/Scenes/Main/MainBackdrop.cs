﻿using Master.Engine;
using Master.Engine.Entities;
using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using MonogameEngine.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Main
{
    class MainBackdrop : GameObject
    {
        private static string[] _textures ={
                "Backgrounds/Seaweed",
                "Backgrounds/Seaweed2",
                "Backgrounds/Rock"
        };

        public MainBackdrop(Scene scene) : base(scene) { }

        public override void Load()
        {
            var globalSpeed = (Scene as SceneMain).BackgroundManager.GlobalSpeed;

            Sprite = new Sprite(Resources.LoadResource<Texture2D>(_textures[Utility.GetRandom(_textures.Length)]));
            Sprite.Scale *= 0.1f + 0.025f * Utility.GetRandom(4);
            Sprite.Origin = new Vector2(0, Sprite.Size.Y);
            Sprite.DrawDepth = DrawDepths.Back - 0.003f;
            Speed = globalSpeed * (0.3f + Sprite.Scale.X);
            Position = new Vector2(Scene.Size.Width, Scene.Size.Height);
            DirectionDegrees = 180.0f;

            OnOutsideScreen += Bubble_OnOutsideScreen;
        }



        private void Bubble_OnOutsideScreen(object sender, EventArgs e)
        {
            Destroy();
        }
    }
}

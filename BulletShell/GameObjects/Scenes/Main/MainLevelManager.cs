﻿using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Main
{
    public class MainLevelManager : GameObject
    {
        #region Constructor
        public MainLevelManager(Scene scene) : base(scene) { }
        #endregion

        #region Members
        private const string _fileLevels = "levels.xml";

        public MainEnemyController enemyController;
        public List<Level> Levels;
        public Level CurrentLevel
        {
            get { return Levels[_currentLevel]; }
        }

        private Timer _timerNextLevel; // TODO: misschien beter gewoon aantallen tellen...
        private int _currentLevel;

        private Text _progressText;
        private int _progress;

        #endregion

        #region Methods
        public override void Load()
        {
            Levels = Utility.LoadFromXML<List<Level>>(_fileLevels);

        }


        public void Reset()
        {
            _progressText?.Destroy();
            _progressText = new Text(Scene);
            _progressText.Value = "";
            _progressText.Position = new Vector2(10,30);
            _progressText.Sprite.Color = Color.DimGray;

            _timerNextLevel?.Stop();
            _timerNextLevel = new Timer();
            _timerNextLevel.Elapsed += _timerNextLevel_Elapsed;
            _timerNextLevel.Interval = 1000;
            _timerNextLevel.Start();
            _currentLevel = -1;
            _progress = 0;

            LevelUp();
            DisplayProgress();
        }


        private void _timerNextLevel_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsActive)
            {
                _progress++;
                DisplayProgress();

                if (_progress == CurrentLevel.DurationInSeconds) { LevelUp(); }
            }
        }

        private void LevelUp()
        {
            if (_currentLevel < Levels.Count - 1)
            {
                _currentLevel++;
                _progress = 0;

                DisplayCurrentLevel();

                (Scene as SceneMain).BackgroundManager.ChangeGlobalSpeed(CurrentLevel.BackgroundSpeed);
                (Scene as SceneMain).EnemyController.StartMakingEnemies();
            }
            else
            {
                _timerNextLevel.Stop();
                _progressText.Value = "";
            }
        }

        private void DisplayCurrentLevel()
        {
            var title = new MainTextCurrentLevel(Scene);
            title.FontName = "levelTitle";
            title.Value = String.Format("Level {0}", _currentLevel + 1);
            title.CenterOrigin();
            title.CenterScreen();
            title.Position -= new Vector2(0, 300);
            title.Sprite.Color = Color.DarkSlateBlue;

            var subtitle = new MainTextCurrentLevel(Scene);
            subtitle.FontName = "levelSubtitle";
            subtitle.Value = CurrentLevel.Name;
            subtitle.CenterOrigin();
            subtitle.CenterScreen();
            subtitle.Position -= new Vector2(0, 350 - GameMaster.TextController.CalculateSize(title.Value, "levelTitle").Y);
            subtitle.Sprite.Color = Color.DarkSlateBlue;
        }

        /// <summary>
        /// ASCII progressbar. Just because we can ;)
        /// </summary>
        private void DisplayProgress()
        {
            var percentageCompleted = ((float)_progress / (float)CurrentLevel.DurationInSeconds) * 100.0f;
            var accuracy = 20;
            var nBars = (int)Math.Floor(percentageCompleted / (100.0f / accuracy));
            var bars = "[".PadRight(nBars + 1, '|');
            var dots = "]".PadLeft(accuracy - nBars + 1, '.');

            _progressText.Value = string.Format("Lvl {0}: {1}{2}", _currentLevel+1, bars,dots);
        }




        #endregion
    }

    public class Level
    {
        public string Name { get; set; }
        public int DurationInSeconds { get; set; }
        public int EnemyIntervalInMilliSecondsMinimum { get; set; }
        public int EnemyIntervalInMilliSecondsMaximum { get; set; }
        public float BackgroundSpeed { get; set; }
        public List<string> EnemyNames { get; set; }
    }
}

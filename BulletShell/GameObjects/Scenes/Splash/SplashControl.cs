﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Splash
{
    class SplashControl : GameObject
    {
        public SplashControl(Scene scene) : base(scene) { }

        private int countdown;

        public override void Load()
        {
            Sprite = new Sprite(Scene.GameMaster.ResourceController.LoadResource<Texture2D>("Backgrounds/Splash"));
            Sprite.Origin = new Vector2(0, 0);
            Position = new Vector2(0, 0);

            // Setting a timer would not work in this case, due to multithreading.
            countdown = 60 * 3; // 60 frames * 5 seconds.

            OnGamepadPress += SplashTimer_OnGamepadPress;
            OnActivate += SplashControl_OnActivate;
            OnKeyPress += SplashTimer_OnKeyPress;
            OnUpdate += SplashControl_OnUpdate;
        }

        private void SplashControl_OnActivate(object sender, EventArgs e)
        {
            GameMaster.SoundController.Loop("menu");
        }

        private void SplashControl_OnUpdate(object sender, UpdateEventArgs e)
        {
            countdown--;

            if (countdown <= 0) { GameMaster.SceneController.ActivateSceneSolo("story"); }
        }

        private void SplashTimer_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape)) { GameMaster.Exit(); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Enter) ||
                e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Space)) { GameMaster.SceneController.ActivateSceneSolo("story"); }
        }

        private void SplashTimer_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Back)) { GameMaster.Exit(); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start) ||
                e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.B)) { GameMaster.SceneController.ActivateSceneSolo("story"); }
        }
    }
}

﻿using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes.Pause;
using Microsoft.Xna.Framework.Graphics;
using BulletShell.GameObjects.Scenes.Hiscores;

namespace BulletShell.GameObjects.Scenes
{
    class SceneHiscores : Scene
    {
        public SceneHiscores(GameMaster game) : base(game)
        {
            var background = new HiscoresBackground(this);
            var display = new HiscoresControl(this);
            var frame = new HiscoresFrame(this);
            var text = new HiscoresText(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonogameEngine;
using Microsoft.Xna.Framework.Graphics;
using BulletShell.GameObjects.Scenes.Menu;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes.Splash;

namespace BulletShell.GameObjects.Scenes
{
    class SceneSplash : Scene
    {
        public SceneSplash(GameMaster game) : base(game)
        {
            Size = new Size(800, 600);

            new SplashControl(this);
        }
        

    }
}

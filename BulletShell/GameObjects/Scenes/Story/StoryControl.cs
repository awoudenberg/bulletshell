﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Story
{
    class StoryControl : GameObject
    {
        public StoryControl(Scene scene) : base(scene) { }


        public override void Load()
        {
            Sprite = new Sprite(Scene.GameMaster.ResourceController.LoadResource<Texture2D>("Backgrounds/Scientist"));
            Sprite.Origin = new Vector2(0, 0);
            Position = new Vector2(0, 0);

            OnGamepadPress += StoryControl_OnGamepadPress; ;
            OnKeyPress += StoryControl_OnKeyPress;
        }

        private void StoryControl_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape) || e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Space)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
        }

        private void StoryControl_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Back) || e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
        }
    }
}

﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Story
{
    class StorySpeechbubble : Text
    {
        public StorySpeechbubble(Scene scene) : base(scene) { }

        private const string _soundBeep = "15720__blackstrobe__e";

        private bool _canStart; // needed because of a bug; directly after activating, the startbutton is still sent to this object, and is set to true!

        private List<string> _text;
        private Timer _timerCharacter;
        private Text _textContinue;
        private bool _readyForNextText;
        private int _indexCharacter;
        private int _indexText;

        private const int CHARACTER_INTERVAL = 20;

        public override void Load()
        {
            base.Load();

            Position = new Vector2(350, 30);

            _text = new List<string>();
            _text.Add("Dag kapitein,\n\nIk heb een topsecretmissie voor jou en je\n" +
                "team. Zoals je weet, zijn onze wateren zwaar\n" +
                "vervuild. Sommige bedrijven dumpen hun afval\n" +
                "in de grote dieptes van de oceanen. We zijn\n" +
                "er zojuist achter gekomen dat Comcor een van\n" +
                "deze bedrijven is.");
            _text.Add("Comcor maakt biochemishe wapens en dumpt hun\n" +
                "afvalvaten zonder pardon in de zee. Enkele\n" +
                "vaten zijn al gaan lekken en hebben alles en\n" +
                "iedereen in het gebied aangetast. De vissen\n" +
                "zijn gemuteerd tot vleesetende roofdieren die\n" +
                "wel 1000 keer zo groot zijn. Ze vallen alles\n" +
                "aan en zijn opweg naar onze kust, in de hoop\n" +
                "er voedsel te vinden.");
            _text.Add("We hebben geprobeerd om deze monsters tegen\n" +
                "te houden door een mijnenveld te plaatsen,\n" +
                "maar niets houdt hen tegen...\n\n" +
                "Jij bent onze enige hoop. Wil je ons helpen?\n" +
                "Kan je de monsters doden voor ze de kust\n" +
                "bereiken en alles en iedereen opvreten?");


            // Het oude verhaal, pure nostalgie :)
            //_text.Add("Ik ontdekte het scheepsjournaal van kapitein\nOrtega in een klooster dat uitkijkt over de\nzee.");
            //_text.Add("In de ochtend van de 16e februari 1634 legde\nOrtega zijn schip voor anker achter een\nkoraalrif.");
            //_text.Add("Dezelfde nacht nog werd hij aangevallen door\npiraten. Het in brand geschoten schip zonk\nop de zeebodem.");
            //_text.Add("Tien dagen later werd de kapitein half\nbewusteloos op het strand gevonden, het\nscheepsjournaal nog in de arm geklemd.");
            //_text.Add("Hij beschreef daarin een wereld van\ngigantische onderwater-wezens, veeeeeeeeeel\ngroter dan zijn duim.");
            //_text.Add("Niemand geloofde hem, maar weet je?\n\nIk denk, dat het verhaal van kapitein Ortega\nbest eens waar kan zijn...");
            
            _readyForNextText = false;
            _indexCharacter = 0;
            _indexText = 0;

            _timerCharacter = new Timer();
            _timerCharacter.Interval = CHARACTER_INTERVAL;
            _timerCharacter.Elapsed += _timerCharacter_Elapsed;

            OnKeyPress += PauseText_OnKeyPress;
            OnActivate += TextPause_OnActivate;
            OnDeactivate += StorySpeechbubble_OnDeactivate;
            OnGamepadPress += TextPause_OnGamepadPress;
        }

        private void StorySpeechbubble_OnDeactivate(object sender, EventArgs e)
        {
            _timerCharacter?.Stop();
        }


        /// <summary>
        /// Advance the position of the last character index, effectively increasing the length of the displayed text.
        /// </summary>
        private void _timerCharacter_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!_readyForNextText && _indexCharacter == _text[_indexText].Length)
            {
                _textContinue = new Text(Scene);
                _textContinue.Value = "-= Druk op Enter/rood =-";
                _textContinue.Sprite.Origin = new Vector2(GameMaster.TextController.CalculateSize(_textContinue.Value).X / 2, -20);
                _textContinue.Position = Position + new Vector2(GameMaster.TextController.CalculateSize(_text[_indexText]).X / 2, GameMaster.TextController.CalculateSize(_text[_indexText]).Y);
            }

            _readyForNextText = (_indexCharacter >= _text[_indexText].Length);
            _indexCharacter = (_indexCharacter < _text[_indexText].Length ? _indexCharacter + 1 : _indexCharacter);

            if(_indexCharacter< _text[_indexText].Length && _indexCharacter%3==0 && _text[_indexText][_indexCharacter]!=' ') { GameMaster.SoundController.Play(_soundBeep); }

            Value = _text[_indexText].Substring(0, _indexCharacter);
        }

        /// <summary>
        /// Go to the next text using enter, if there is one; start the main game if there isn't. Space skips all the text.
        /// </summary>
        private void PauseText_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Enter) && _readyForNextText) { ChangeText(); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Escape)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Keys.Contains(Microsoft.Xna.Framework.Input.Keys.Space)) { StartMainGame(); }
        }

        /// <summary>
        /// Go to the next text using enter, if there is one; start the main game if there isn't. Space skips all the text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextPause_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Start)) { StartMainGame(); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Buttons.Contains(Microsoft.Xna.Framework.Input.Buttons.B) && _readyForNextText) { ChangeText(); }
        }


        private void ChangeText()
        {
            if (_indexText < _text.Count - 1)
            {
                _indexCharacter = 0;
                _indexText++;
                _textContinue?.Destroy();
            }
            else
            {
                GameMaster.SceneController.ActivateSceneSolo("menu");
            }
        }


        private void StartMainGame()
        {
            if (!_canStart)
            {
                _canStart = true;
            }
            else
            {
                GameMaster.SceneController.ActivateSceneSolo("menu");
            }
        }

        private void TextPause_OnActivate(object sender, EventArgs e)
        {
            _timerCharacter.Start();
            _canStart = false;
        }
    }
}

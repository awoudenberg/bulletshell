﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonogameEngine;
using Microsoft.Xna.Framework.Graphics;
using BulletShell.GameObjects.Scenes.Menu;
using Master.Engine.Entities;
using Master.Engine.Enums;

namespace BulletShell.GameObjects.Scenes
{
    class SceneMenu : Scene
    {
        public SceneMenu(GameMaster game) : base(game)
        {
            Size = new Size(800, 600);

            var background = new GameObject(this);
            var octopus = new MenuOctopus(this);
            var title = new MenuTitleManager(this);

            background.Sprite = new Sprite(game.ResourceController.LoadResource<Texture2D>("backgrounds/menu"));
            background.Sprite.DrawDepth = DrawDepths.Back;
            background.Sprite.Origin = new Vector2(0, 0);
            background.Position = new Vector2(0, 0);
        }

        

    }
}

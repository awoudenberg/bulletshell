﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonogameEngine;
using Microsoft.Xna.Framework.Graphics;
using BulletShell.GameObjects.Scenes.Menu;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes.Splash;
using BulletShell.GameObjects.Scenes.Story;
using BulletShell.GameObjects.Scenes.Settings;

namespace BulletShell.GameObjects.Scenes
{
    class SceneSettings: Scene
    {
        public SceneSettings(GameMaster game) : base(game)
        {
            Size = new Size(800, 600);

            new SettingsBackground(this);
            new SettingsControl(this);
            new SettingsText(this);
        }
        

    }
}

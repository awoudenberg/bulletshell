﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Scenes.Settings
{
    class SettingsControl : GameObject
    {

        private const string _spriteHighlight = "Sprites/Settings/SettingsOptionHighlight";
        private const string _soundChangeSetting = "96125__bmaczero__bubbles-single2";

        private int _currentOption = 0;
        private Color _colorSelected = Color.AliceBlue;
        private Color _colorNormal = Color.Black;

        public SettingsControl(Scene scene) : base(scene) { }


        private Dictionary<int, List<string>> _options;
        private List<Text> _texts;


        public override void Load()
        {
            Sprite = new Sprite(GameMaster.ResourceController.LoadResource<Texture2D>(_spriteHighlight));
            Sprite.Origin = new Vector2(0, 0);

            PositionStart = new Vector2(50, 120);

            _options = new Dictionary<int, List<string>>();
            _options.Add(0, Enumerable.Range(0, 11).Select(o => string.Format("Volume:          {0}", (10 * o))).ToList()); // volume, 0 - 100, then converted to strings. Love it.
            _options.Add(1, new List<string> { "Muziek:          uit", "Muziek:          aan" }); // use music
            _options.Add(2, new List<string> { "Geluidseffecten: uit", "Geluidseffecten: aan" }); // use soundeffects
            _options.Add(3, new List<string> { "Vrij bewegen:    uit", "Vrij bewegen:    aan" }); // Boot vrij bewegen
            _options.Add(4, Enumerable.Range(0, 4).Select(o => string.Format("Bootbellen:      {0}", o)).ToList()); // number of bubbles from the boat
            _options.Add(5, Enumerable.Range(0, 3).Select(o => string.Format("Vijandbellen:    {0}", 10 * o)).ToList()); // Number of bubbles fmro the enemies

            _texts = new List<Text>();
            for (int i = 0; i < _options.Count; i++)
            {
                var text = new Text(Scene);

                text.Position = PositionStart + new Vector2(10, 3 + i * Sprite.Size.Y);
                text.Value = "Option " + i;
                text.Sprite.Color = _colorNormal;

                _texts.Add(text);
            }

            _texts[0].Sprite.Color = _colorSelected;

            UpdateTexts();

            OnGamepadPress += SettingsControl_OnGamepadPress;
            OnActivate += SettingsControl_OnActivate;
            OnKeyPress += SettingsControl_OnKeyPress;
        }

        private void SettingsControl_OnActivate(object sender, EventArgs e)
        {
            GameMaster.SoundController.Loop("menu");
        }

        private void UpdateTexts()
        {
            var settings = GameMaster.SettingController.Settings;

            _texts[0].Value = _options[0][(int)Math.Round(10 * settings.MasterVolume)];
            _texts[1].Value = _options[1][(settings.UseMusic ? 1 : 0)];
            _texts[2].Value = _options[2][(settings.UseSoundEffects ? 1 : 0)];
            _texts[3].Value = _options[3][(settings.FreeMovement ? 1 : 0)];
            _texts[4].Value = _options[4][settings.AmountOfBubblesBoat];
            _texts[5].Value = _options[5][(int)Math.Round(settings.AmountOfBubblesEnemy/10.0f)];
        }

        private void SettingsControl_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            _texts[_currentOption].Sprite.Color = _colorNormal;

            if (e.Keys.Contains(Keys.Escape) || e.Keys.Contains(Keys.Space)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Keys.Contains(Keys.Down)) { _currentOption = (_currentOption + 1) % _options.Count; GameMaster.SoundController.Play(_soundChangeSetting); } // one down
            if (e.Keys.Contains(Keys.Up)) { _currentOption = (_currentOption + _options.Count - 1) % _options.Count; GameMaster.SoundController.Play(_soundChangeSetting); } // one up
            if (e.Keys.Contains(Keys.Right)) { AlterOption(_currentOption, 1); GameMaster.SoundController.Play(_soundChangeSetting); }
            if (e.Keys.Contains(Keys.Left)) { AlterOption(_currentOption, -1); GameMaster.SoundController.Play(_soundChangeSetting); }

            _texts[_currentOption].Sprite.Color = _colorSelected;

            Position = PositionStart + new Vector2(0, _currentOption * Sprite.Size.Y);
        }

        private void AlterOption(int option, int direction)
        {
            var settings = GameMaster.SettingController.Settings;

            switch (_currentOption)
            {
                case 0: settings.MasterVolume += 0.1f * direction; break;
                case 1: settings.UseMusic = !settings.UseMusic; break;
                case 2: settings.UseSoundEffects = !settings.UseSoundEffects; break;
                case 3: settings.FreeMovement = !settings.FreeMovement; break;
                case 4: settings.AmountOfBubblesBoat += direction; break;
                case 5: settings.AmountOfBubblesEnemy += 10 * direction; break;

                default:
                    break;
            }

            GameMaster.SettingController.PersistSettings();
            UpdateTexts();
        }

        private void SettingsControl_OnGamepadPress(object sender, MonogameEngine.Controllers.GamepadPressedEventArgs e)
        {
            _texts[_currentOption].Sprite.Color = _colorNormal;

            if (e.Buttons.Contains(Buttons.Back)) { GameMaster.SceneController.ActivateSceneSolo("menu"); }
            if (e.Buttons.Contains(Buttons.DPadDown)) { _currentOption = (_currentOption + 1) % _options.Count; GameMaster.SoundController.Play(_soundChangeSetting); } // one down
            if (e.Buttons.Contains(Buttons.DPadUp)) { _currentOption = (_currentOption + _options.Count - 1) % _options.Count; GameMaster.SoundController.Play(_soundChangeSetting); } // one up
            if (e.Buttons.Contains(Buttons.DPadRight)) { AlterOption(_currentOption, 1); GameMaster.SoundController.Play(_soundChangeSetting); }
            if (e.Buttons.Contains(Buttons.DPadLeft)) { AlterOption(_currentOption, -1); GameMaster.SoundController.Play(_soundChangeSetting); }

            _texts[_currentOption].Sprite.Color = _colorSelected;

            Position = PositionStart + new Vector2(0, _currentOption * Sprite.Size.Y);
        }
    }
}

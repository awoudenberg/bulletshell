﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Scenes.Settings
{
    class SettingsText : Text
    {
        public SettingsText(Scene scene) : base(scene) { }


        public override void Load()
        {
            base.Load();

            Sprite.Color = Color.AliceBlue;
            Value = "-= Druk op een Escape/Back om terug te gaan =-";

            OnUpdate += TextPause_OnUpdate;
            OnActivate += TextPause_OnActivate;
        }


        private void TextPause_OnUpdate(object sender, MonogameEngine.UpdateEventArgs e)
        {
            if (Position.Y > PositionStart.Y) { Jump(); }
        }

        private void Jump()
        {
            DirectionDegrees = 90;
            Speed = 3;
            Gravity = 0.2f;
        }

        private void TextPause_OnActivate(object sender, EventArgs e)
        {
            CenterScreen();
            Position += new Vector2(0, 1250);
            PlaceWithinScreen();
            PositionStart = Position;

            Position -= new Vector2(0, 50);
            DirectionDegrees = 90;
            Gravity = 0.2f;
            Speed = 0;
        }
    }
}

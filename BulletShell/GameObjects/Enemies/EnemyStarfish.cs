﻿using BulletShell.GameObjects.Bullets;
using Master.Engine.Entities;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Enemies
{
    class EnemyStarfish : Enemy
    {
        Timer _bulletTimer;
        float turnSpeed;
        float turnSpeedMax;
        float turnSpeedIncrement;

        public EnemyStarfish(Scene scene) : base(scene) { }

        public override void Load()
        {
            base.Load();

            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Strip Starfish"), 4, 1, 10);
            PlaceOutsideScreen();

            Health = 50;
            Points = 100;

            _bulletTimer = new Timer();
            _bulletTimer.Interval = 500;
            _bulletTimer.Elapsed += ShootBullets;
            _bulletTimer.Start();

            turnSpeed = 0;
            turnSpeedMax = 20;
            turnSpeedIncrement = 0.10f;

            OnUpdate += Starfish_OnUpdate;
            OnDestroy += EnemyStarfish_OnDestroy;
        }


        private void EnemyStarfish_OnDestroy(object sender, EventArgs e)
        {
            _bulletTimer.Stop();
        }

        private void ShootBullets(object sender, ElapsedEventArgs e)
        {
            if (IsActive)
            {
                for (int i = 0; i < 5; i++)
                {
                    var bullet = new EnemyBullet(Scene);

                    bullet.Position = Position;
                    bullet.Speed = 10;
                    bullet.DirectionDegrees = 72 * i + Sprite.Rotation + 90;
                }
            }
        }

        private void Starfish_OnUpdate(object sender, UpdateEventArgs e)
        {
            turnSpeed += (turnSpeed + turnSpeedIncrement > turnSpeedMax ? 0 : turnSpeedIncrement);

            Sprite.Rotation += turnSpeed;
        }
    }
}

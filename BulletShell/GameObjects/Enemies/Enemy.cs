﻿using BulletShell.GameObjects.Bullets;
using MonogameEngine.Controllers;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes;
using BulletShell.GameObjects.Scenes.Main;
using Master.Engine;
using Microsoft.Xna.Framework;

namespace BulletShell.GameObjects.Enemies
{
    class Enemy : GameObject
    {
        private const string _soundBubbles = "96125__bmaczero__bubbles-single2";
        private const string _soundHit = "215162__otisjames__thud";

        public int Health = 1;
        public int Points = 10;

        public Enemy(Scene scene) : base(scene) { }

        public override void Load()
        {
            base.Load();

            Speed = (Scene as SceneMain).BackgroundManager.GlobalSpeed;
            DirectionDegrees = 180;

            CheckCollisionsWith.Add(typeof(MainSubmarine));
            CheckCollisionsWith.Add(typeof(PlayerBullet));

            OnDestroy += Enemy_OnDestroy;
            OnCollision += Enemy_OnCollision;
            OnOutsideScreen += Enemy_OnOutsideScreen;
        }

        private void Enemy_OnOutsideScreen(object sender, EventArgs e)
        {
            if (Position.X < 0) { Destroy(); }
        }

        private void Enemy_OnCollision(object sender, CollisionEventArgs e)
        {
            for (int i = 0; i < e.Instances.Count; i++)
            {
                if (e.Instances[i].GetType() == typeof(MainSubmarine) && !(e.Instances[i] as MainSubmarine).IsInvincible)
                {
                    (e.Instances[i] as MainSubmarine).HitByEnemy();
                    (Scene as SceneMain).LifeManager.LoseLife();
                    Destroy();
                }

                if (e.Instances[i].GetType() == typeof(PlayerBullet) && Position.X <= Scene.Size.Width)
                {
                    GameMaster.SoundController.Play(_soundHit, Utility.CalculatePan(Position));
                    DecreaseHealth();
                    e.Instances[i].Destroy(); // Destroy the bullet on contact.
                }
            }
        }

        private void Enemy_OnDestroy(object sender, EventArgs e)
        {
            if (!IsForceDestroyed)
            {
                GameMaster.SoundController.Play(_soundBubbles);
                GameMaster.ScoreController.AwardPoints(Position, Points);
                CreateBubbles();
            }
        }

        public void DecreaseHealth(int Damage = 10)
        {
            Health -= Damage;

            if (Health < 0) { Destroy(); }
        }


        public void CreateBubbles()
        {
            // Create some bubbles.
            for (int i = 0; i < GameMaster.SettingController.Settings.AmountOfBubblesEnemy; i++)
            {
                var bubble = new MainBubble(Scene);

                bubble.Position = Utility.GetRandomVector2(Position, Sprite.Size);
                bubble.DirectionDegrees = Utility.GetRandom(180);
                bubble.Speed = (float)Utility.GetRandom(3) / 5 + 1.0f - 2.5f;
            }
        }

        public void PlaceOutsideScreen()
        {
            Position = new Vector2(GameMaster.Size.Width + 100, Utility.GetRandom((int)Sprite.Size.Y, (int)(Scene.Size.Height - Sprite.Size.Y)));
            PlaceWithinScreen();
            Position += new Vector2(Sprite.Size.X, 0);
        }

    }
}

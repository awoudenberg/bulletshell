﻿using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects
{
    class Octopus : GameObject
    {
        Sprite Head;

        public Octopus(GameMaster game) : base(game)
        {
            Sprite = new Sprite((Texture2D)Resources.LoadResource<Texture2D>("OctopusTentakels"));
            Head = new Sprite((Texture2D)Resources.LoadResource<Texture2D>("Octopushead"));

            Sprite.Scale *= 0.5f;
            Head.Scale *= 0.5f;

            OnUpdate += Octopus_OnUpdate;
            OnAfterDraw += Octopus_OnAfterDraw;
        }

        private void Octopus_OnUpdate(object sender, UpdateEventArgs e)
        {
            Sprite.Rotation += 5;
        }

        private void Octopus_OnAfterDraw(object sender, DrawEventArgs e)
        {
            Head.Draw(e.SpriteBatch, Position);
        }
    }
}

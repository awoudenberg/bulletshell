﻿using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Enemies
{
    class EnemyFish : Enemy
    {
        private string[] _textures =
        {
            "Sprites/Enemies/fishBlue",
            "Sprites/Enemies/fishDark",
            "Sprites/Enemies/fishPink",
            "Sprites/Enemies/fishRed",
            "Sprites/Enemies/fishYellow"
        };

        public EnemyFish(Scene scene) : base(scene)
        {
            var texture = _textures[Utility.GetRandom(_textures.Length)];

            Sprite = new Sprite(Resources.LoadResource<Texture2D>(texture), 4, 1, 15);
            PlaceOutsideScreen();

            Health = 30;
        }
    }
}

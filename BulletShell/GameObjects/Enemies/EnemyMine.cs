﻿using BulletShell.GameObjects.Scenes;
using BulletShell.GameObjects.Scenes.Main;
using Master.Engine;
using Master.Engine.Entities;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using MonogameEngine.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Enemies
{
    class EnemyMine : Enemy
    {
        bool causeExplosion = false;
        SoundEffect sndExplosion;

        public EnemyMine(Scene scene) : base(scene) { }

        public override void Load()
        {
            base.Load();

            sndExplosion = Resources.LoadResource<SoundEffect>("sndExplosion");
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Sprites/Mine"));
            PlaceOutsideScreen();

            Points = 150;

            OnCollision += Mine_OnCollision;
            OnDestroy += Mine_OnDestroy;
        }


        private void Mine_OnCollision(object sender, CollisionEventArgs e)
        {
            causeExplosion = true;
            Destroy();
        }

        private void Mine_OnDestroy(object sender, EventArgs e)
        {
            if (causeExplosion)
            {
                //Create the soundeffect- can't have an exploding mine without an explosion.
                GameMaster.SoundController.Play(sndExplosion, Utility.CalculatePan(Position), 0.5f);


                // Create the spikes.
                for (int i = 0; i < 6; i++)
                {
                    var minePiece = new MinePiece(Scene);

                    minePiece.Position = Position;
                    minePiece.DirectionDegrees = 60 * i;
                    minePiece.Sprite.Rotation = -60 * i;
                }
            }
        }

       
    }


    class MinePiece : GameObject
    {
        public MinePiece(Scene scene) : base(scene) { }

        public override void Load()
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Sprites/Spike"));
            Speed = 16;

            CheckCollisionsWith.Add(typeof(MainSubmarine));

            OnCollision += MinePiece_OnCollision; ;

            OnOutsideScreen += MinePiece_OnOutsideScreen;
        }

        private void MinePiece_OnCollision(object sender, CollisionEventArgs e)
        {
            for (int i = 0; i < e.Instances.Count; i++)
            {
                if (e.Instances[i].GetType() == typeof(MainSubmarine) && !(e.Instances[i] as MainSubmarine).IsInvincible)
                {
                    Destroy();
                    (e.Instances[i] as MainSubmarine).HitByEnemy();
                    (Scene as SceneMain).LifeManager.LoseLife();
                }
            }
        }

        private void MinePiece_OnOutsideScreen(object sender, EventArgs e)
        {
            Destroy();
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects.Enemies
{
    class Shell : GameObject
    {
        bool _keepClosed;
        Random _rng = new Random();
        Timer _timerOpen;

        public Shell(GameMaster game) : base(game)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("shell"), 4, 1, 0);
            Sprite.SpriteEffects = SpriteEffects.FlipHorizontally;
            Sprite.Origin = new Vector2(0, Sprite.Size.Y);
            Sprite.Scale *= 0.5f;
            Sprite.CurrentFrame = 2;

            _keepClosed = false;
            _timerOpen = new Timer();
            _timerOpen.Elapsed += _timerOpen_Elapsed;
            _timerOpen.Interval = 500;
            _timerOpen.Start();

            Position = new Vector2(Position.X, game.Size.Height + 20);

            OnUpdate += Shell_OnUpdate;
            OnKeyPress += Shell_OnKeyPress;
        }

        private void _timerOpen_Elapsed(object sender, ElapsedEventArgs e)
        {
            OpenShell();
        }

        private void Shell_OnKeyPress(object sender, MonogameEngine.Controllers.KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Keys.O) && Sprite.CurrentFrame == 2) { OpenShell(); }
        }

        private void OpenShell()
        {
            Sprite.CurrentFrame = (Sprite.CurrentFrame + 1) % Sprite.Frames.Count;
            Sprite.Speed = 5;
            _keepClosed = false;

            var pearl = new Pearl(GameMaster);
            pearl.Position = new Vector2(Position.X, Position.Y - 60);
            pearl.PositionTarget = new Vector2(0, _rng.Next(0, GameMaster.Size.Height-50));
        }

        private void Shell_OnUpdate(object sender, UpdateEventArgs e)
        {
            if (Sprite.CurrentFrame == 0) { _keepClosed = true; }
            if (_keepClosed && Sprite.CurrentFrame == 2) { Sprite.Speed = 0; Sprite.CurrentFrame = 2; }
        }
    }

    public class Pearl : GameObject
    {
        public Pearl(GameMaster game) : base(game)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("pearl"));
            PositionTarget = new Vector2(0, 0);
            Speed = 8;

            OnOutsideScreen += Pearl_OnOutsideScreen;
        }

        private void Pearl_OnOutsideScreen(object sender, EventArgs e)
        {
            Destroy();
        }
    }
}

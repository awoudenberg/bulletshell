﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects.Enemies
{
    class EnemyAnglerfish : Enemy
    {
        public EnemyAnglerfish(Scene scene) : base(scene)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Sprites/Enemies/anglerfish"), 4, 1, 15);
            PlaceOutsideScreen();

            Health = 20;
        }
    }
}

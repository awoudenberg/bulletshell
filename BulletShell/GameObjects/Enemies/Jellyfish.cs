﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BulletShell.GameObjects
{
    class Jellyfish : GameObject
    {
        Timer timerFloat;

        public Jellyfish(GameMaster game) : base(game)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("jellyfish"), 4, 1, 10);

            Sprite.Rotation -= 70;
            Gravity = 0.01f;

            timerFloat = new Timer();
            timerFloat.Interval = 1800;
            timerFloat.Elapsed += TimerFloat_Elapsed;
            timerFloat.Start();
        }

        private void TimerFloat_Elapsed(object sender, ElapsedEventArgs e)
        {
            DirectionVector = new Vector2(DirectionVector.X, -0.5f);
        }
        
    }
}

﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletShell.GameObjects
{
    public class Bullet : GameObject
    {

        public Bullet(Scene scene) : base(scene){}

        public override void Load()
        {
            Sprite = new Sprite((Texture2D)Resources.LoadResource<Texture2D>("Strip Fireball 94%"), 4, 1, 0);
            Speed = 12;

            OnOutsideScreen += Bullet_OnOutsideScreen;
        }


        private void Bullet_OnOutsideScreen(object sender, EventArgs e)
        {
            Destroy();
        }

    }
}

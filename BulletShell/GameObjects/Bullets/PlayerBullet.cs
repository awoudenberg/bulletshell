﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonogameEngine;
using BulletShell.GameObjects.Enemies;
using Master.Engine.Entities;

namespace BulletShell.GameObjects.Bullets
{
    public class PlayerBullet : Bullet
    {
        public PlayerBullet(Scene scene) : base(scene){}

        public override void Load()
        {
            base.Load();

            Sprite.Scale *= 0.5f;
        }
    }
}

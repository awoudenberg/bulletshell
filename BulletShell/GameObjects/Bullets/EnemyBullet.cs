﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonogameEngine;
using BulletShell.GameObjects.Enemies;
using Master.Engine.Entities;
using BulletShell.GameObjects.Scenes.Main;
using BulletShell.GameObjects.Scenes;

namespace BulletShell.GameObjects.Bullets
{
    public class EnemyBullet : Bullet
    {
        public EnemyBullet(Scene scene) : base(scene) { }

        public override void Load()
        {
            base.Load();

            CheckCollisionsWith.Add(typeof(MainSubmarine));

            OnCollision += EnemyBullet_OnCollision;
        }

        private void EnemyBullet_OnCollision(object sender, CollisionEventArgs e)
        {
            for (int i = 0; i < e.Instances.Count; i++)
            {
                if (e.Instances[i].GetType() == typeof(MainSubmarine) && !(e.Instances[i] as MainSubmarine).IsInvincible)
                {
                    Destroy();
                    (e.Instances[i] as MainSubmarine).HitByEnemy();
                    (Scene as SceneMain).LifeManager.LoseLife();
                }
            }
        }
    }
}

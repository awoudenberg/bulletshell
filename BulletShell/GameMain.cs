﻿using BulletShell.GameObjects;
using BulletShell.GameObjects.Enemies;
using BulletShell.GameObjects.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonogameEngine;
using System.Threading;

namespace BulletShell
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMain : GameMaster
    {
        public GameMain() : base() { }

        protected override void LoadContent()
        {
            base.LoadContent();

            SceneController.AddScene("main", new SceneMain(this));
            SceneController.AddScene("menu", new SceneMenu(this));
            SceneController.AddScene("pause", new ScenePause(this));
            SceneController.AddScene("story", new SceneStory(this));
            SceneController.AddScene("splash", new SceneSplash(this));
            SceneController.AddScene("hiscores", new SceneHiscores(this));
            SceneController.AddScene("settings", new SceneSettings(this));

            SceneController.ActivateSceneSolo("splash");
        }


    }
}

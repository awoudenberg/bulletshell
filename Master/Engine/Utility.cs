﻿using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Master.Engine
{
    public static class Utility
    {
        public static GameMaster Game { get; set; }

        private static Random rng = new Random();

        public static int GetRandom()
        {
            return rng.Next();
        }

        public static int GetRandom(int maxValue)
        {
            return rng.Next(maxValue);
        }

        public static int GetRandom(int minValue, int maxValue)
        {
            return rng.Next(minValue, maxValue);
        }

        public static Vector2 AngleToVector(float angle)
        {
            return new Vector2((float)Math.Cos(MathHelper.ToRadians(angle)), -(float)Math.Sin(MathHelper.ToRadians(angle)));
        }

        public static float VectorToAngle(Vector2 vector)
        {
            return MathHelper.ToDegrees((float)Math.Atan2(vector.Y, vector.X));
        }

        /// <summary>
        /// Destroys every gameobject in the list, then clears the list.
        /// </summary>
        public static void DestroyAll<T>(List<T> gameObjects)
        {
            foreach (T gameObject in gameObjects) { (gameObject as GameObject).Destroy(); }

            gameObjects.Clear();
        }

        public static float Limit(float value, float lower, float upper)
        {
            if (value < lower) { value = lower; }
            if (value > upper) { value = upper; }

            return value;
        }

        public static void SaveToXML<T>(T content, string location)
        {
            var ser = new XmlSerializer(typeof(T));

            using (var fs = new FileStream(location, FileMode.Create))
            {
                ser.Serialize(fs, content);
            }
        }

        public static T LoadFromXML<T>(string location)
        {
            var serializer = new XmlSerializer(typeof(T));
            var reader = new StreamReader(location);
            var result = ((T)serializer
                .Deserialize(reader));

            reader.Close();

            return result;
        }



        /// <summary>
        /// Calculates how far left or right an object is, counting from the center of the screen. The sides of the window
        /// are -1 (left) and 1 (right). Centerscreen is 0; anything in between is a fraction between 0 and either side.
        /// This is used for positional audio.
        /// </summary>
        public static float CalculatePan(Vector2 position)
        {
            if (Game == null) { return 0; }

            var half = Game.Size.Width / 2;
            var result = (Game == null ? 0 : (position.X - half) / half);

            return result;
        }

        /// <summary>
        /// Picks a random point within the inner n% of the biggest circle around Position
        /// that can be fully drawn on the sprite with a certain size. Used to create bubbles
        /// at random positions when an enemy explodes.
        /// </summary>
        public static Vector2 GetRandomVector2(Vector2 position, Vector2 size, int maxRadiusFactor = 30)
        {
            var smallestRadius = (size.X < size.Y ? size.X : size.Y) / 2.0f;
            var radiusFactor = GetRandom(maxRadiusFactor) / 100.0f;
            var angle = GetRandom(360);
            var y = radiusFactor * smallestRadius * (float)Math.Sin(MathHelper.ToRadians(angle));
            var x = radiusFactor * smallestRadius * (float)Math.Cos(MathHelper.ToRadians(angle));
            var result = position + new Vector2(x, y);

            return result;
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonogameEngine.Controllers;
using Master.Engine;
using System.Timers;
using Master.Engine.Enums;
using Master.Engine.Entities;

namespace MonogameEngine.Entities
{
    class Points : GameObject
    {
        private string _prefix = "";

        public int Value = 10;
        public bool DestroyAfterInterval
        {
            set
            {
                if (value)
                {
                    _timerDestroy.Start();
                }
                else
                {
                    _prefix = "Score: ";
                    _timerDestroy.Stop();
                }
            }
        }

        // TODO: could be overkill, this timer- could work with a counter.
        private Timer _timerDestroy;

        public Points(Scene scene) : base(scene) { }

        public override void Load()
        {
            _timerDestroy = new Timer();
            _timerDestroy.Interval = 1000;
            _timerDestroy.Elapsed += _timerDestroy_Elapsed;

            Sprite.DrawDepth = DrawDepths.Front;

            DestroyAfterInterval = true;

            OnAfterDraw += Points_OnAfterDraw;
        }


        private void _timerDestroy_Elapsed(object sender, ElapsedEventArgs e)
        {
            Destroy();
        }


        private void Points_OnAfterDraw(object sender, DrawEventArgs e)
        {
            GameMaster.TextController.Write(Position, string.Format("{0}{1}", _prefix, Value), Color.DimGray);
        }



    }

}

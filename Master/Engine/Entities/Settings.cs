﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Entities
{
    public class Settings
    {
        public float MasterVolume { get; set; }
        public bool UseMusic { get; set; }
        public bool UseSoundEffects { get; set; }

        public int AmountOfBubblesBoat { get; set; }
        public int AmountOfBubblesEnemy { get; set; }
        public bool FreeMovement { get; set; }
    }
}

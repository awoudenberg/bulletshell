﻿using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Entities
{
    /// <summary>
    /// Represents a type of screen within the game, such as the main menu, the splashscreen, or the main game.
    /// </summary>
    public class Scene : DrawableGameComponent
    {
        private List<GameObject> _gameObjects;
        private GameMaster _gameMaster;
        private Object _criticalCodeLock = new object();
        private Size _size;
        private bool _isActive;
        private bool _isVisible;

        public Object CriticalCodeLock
        {
            get { return _criticalCodeLock; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                UpdateGameObjects();
            }
        }
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                UpdateGameObjects();
            }
        }

        public bool IsSolo { get; set; }
        public bool IsPaused { get; set; }

        public Size Size
        {
            get { return (_size == null ? new Size(0, 0) : _size); }
            set { _size = value; }
        }

        public SceneStates State
        {
            set
            {
                IsActive = (value == SceneStates.InvisibleActive || value == SceneStates.VisibleActive);
                IsVisible = (value == SceneStates.VisibleActive || value == SceneStates.VisibleInactive);
            }

            get
            {
                SceneStates state = SceneStates.InvisibleInactive;

                if (IsVisible && IsActive) { state = SceneStates.VisibleActive; }
                if (IsVisible && !IsActive) { state = SceneStates.VisibleInactive; }
                if (!IsVisible && IsActive) { state = SceneStates.InvisibleActive; }
                if (!IsVisible && !IsActive) { state = SceneStates.InvisibleInactive; }

                return state;
            }
        }



        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="game"></param>
        public Scene(GameMaster game) : base(game)
        {
            _gameObjects = new List<GameObject>();
            _gameMaster = game;

            State = SceneStates.InvisibleInactive;
            IsPaused = false;
        }

        public void SetState(SceneStates state)
        {
            State = state;

            UpdateGameObjects();
        }

        private void UpdateGameObjects()
        {
            lock (_criticalCodeLock)
            {
                try
                {

                    foreach (var gameObject in _gameObjects)
                    {
                        gameObject.State = State;
                    }
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// Draw the scene and all its gameobjects, but only if the scene is marked as visible.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            if (IsVisible) { base.Draw(gameTime); }
        }

        public void AddGameObject(GameObject gameObject)
        {
            gameObject.State = State;

            lock (_criticalCodeLock)
            {
                _gameObjects.Add(gameObject);
            }
        }

        public GameMaster GameMaster
        {
            get { return _gameMaster; }
        }

        

        internal void RemoveGameObject(GameObject gameObject)
        {
            lock (_criticalCodeLock)
            {
                _gameObjects.Remove(gameObject);
            }
        }

        public List<GameObject> GetAllOfType(Type type)
        {
            List<GameObject> gameObjects;

            lock (_criticalCodeLock)
            {
                gameObjects = new List<GameObject>();

                foreach (var gameObject in _gameObjects)
                {
                    if (gameObject.GetType() == type || gameObject.GetType().IsSubclassOf(type))
                    {
                        gameObjects.Add(gameObject);
                    }
                }
            }

            return gameObjects;
        }
    }

}

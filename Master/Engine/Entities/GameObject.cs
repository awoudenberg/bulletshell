﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonogameEngine.Controllers;
using Master.Engine;
using Master.Engine.Entities;
using Master.Engine.Enums;

namespace MonogameEngine
{
    /// <summary>
    /// Extended event-driven gameobject within Monogame. 
    /// 
    /// Author: Anthony Woudenberg, anthony.woudenberg@gmail.com
    /// </summary>
    public partial class GameObject : DrawableGameComponent
    {
        // Standard fields
        //public int ID { get; set; } // not yet used.

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _positionPrevious = _position;
                _position = value;

                if (_positionPrevious != _position) { PositionChanged(); }
            }
        }
        public Vector2 PositionPrevious
        {
            get { return _positionPrevious; }
        }
        public Vector2 PositionStart
        {
            get { return _positionStart; }
            set
            {
                _positionStart = value;
                _positionPrevious = value;
                _position = value;
            }
        }
        public Vector2 PositionTarget // Mind you; only works when the object has a speed.
        {
            get { return _positionTarget; }
            set
            {
                float _directionPrevious = _directionDegrees;

                _positionTarget = value;
                _directionDegrees = (float)(Math.Atan2(_position.Y - _positionTarget.Y, _positionTarget.X - _position.X) * (180.0f / Math.PI));
                _directionVector.X = _speed * (float)Math.Cos(_directionDegrees * Math.PI / 180);
                _directionVector.Y = -_speed * (float)Math.Sin(_directionDegrees * Math.PI / 180);

                if (_directionDegrees != _directionPrevious) { DirectionChanged(); }
            }
        }


        public Vector2 DirectionVector
        {
            get { return _directionVector; }
            set
            {
                float _directionPrevious = _directionDegrees;

                _directionVector = value;
                _directionDegrees = (float)(Math.Asin(value.Y / _speed) * (180.0f / Math.PI));
                _speed = (float)Math.Sqrt(Math.Pow(value.X, 2) + Math.Pow(value.Y, 2));

                if (_directionDegrees != _directionPrevious) { DirectionChanged(); }
            }
        }
        public float DirectionDegrees
        {
            get { return _directionDegrees; }
            set
            {
                float _directionPrevious = _directionDegrees;

                _directionDegrees = value;
                _directionVector.X = (float)Math.Round(_speed * (float)Math.Cos(_directionDegrees * Math.PI / 180), 2);
                _directionVector.Y = -(float)Math.Round(_speed * (float)Math.Sin(_directionDegrees * Math.PI / 180), 2);

                if (_directionDegrees != _directionPrevious) { DirectionChanged(); }
            }
        }
        public float Speed
        {
            get { return _speed; }
            set
            {
                var _speedPrevious = _speed;

                _speed = value;
                _directionVector.X = (float)Math.Round(_speed * (float)Math.Cos(_directionDegrees * Math.PI / 180), 2);
                _directionVector.Y = -(float)Math.Round(_speed * (float)Math.Sin(_directionDegrees * Math.PI / 180), 2);

                if (_speed != _speedPrevious) { SpeedChanged(); }
                //OnSpeedChanged();
            }
        }
        public float Gravity;
        public bool IsAlive
        {
            get { return _isAlive; }
        }

        public Sprite Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }
        public GameMaster GameMaster
        {
            get { return _game; }
        }

        public SceneStates State
        {
            set
            {
                IsActive = (value == SceneStates.InvisibleActive || value == SceneStates.VisibleActive);
                IsVisible = (value == SceneStates.VisibleActive || value == SceneStates.VisibleInactive);
            }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (!_isActive && value) { Activated(); } else 
                if (_isActive && !value) { Deactivated(); }


                    _isActive = value;
            }
        }
        public bool IsVisible { get; set; }
        public bool IsForceDestroyed { get { return _isForceDestroyed; } set { _isForceDestroyed = value; } }
        public Scene Scene { get; set; }

        public InputController Input
        {
            get { return _input; }
        }
        public ResourceController Resources
        {
            get { return _resources; }
        }

        public List<Type> CheckCollisionsWith; // each instance will check collisions with instances of the types in this list, and react accordingly.

        //public float MaxSpeed; // yet unused.
        //public float Acceleration; // yet unused.

        // Eventhandlers. Derived objects can hook into these events to react whenever needed.
        public event EventHandler OnInitialise;
        public event EventHandler OnPositionChange;
        public event EventHandler OnDirectionChange;
        public event EventHandler OnSpeedChange;
        public event EventHandler OnDestroy;
        public event EventHandler OnOutsideScreen;
        public event EventHandler OnActivate;
        public event EventHandler OnDeactivate;
        public event EventHandler<UpdateEventArgs> OnUpdate;
        public event EventHandler<DrawEventArgs> OnBeforeDraw;
        public event EventHandler<DrawEventArgs> OnAfterDraw;
        public event EventHandler<CollisionEventArgs> OnCollision;

        public event EventHandler<KeyboardChangedEventArgs> OnKeyChange;
        public event EventHandler<KeyboardPressedEventArgs> OnKeyPress;
        public event EventHandler<KeyboardDownEventArgs> OnKeyDown;
        public event EventHandler<KeyboardReleasedEventArgs> OnKeyRelease;

        public event EventHandler<GamepadChangedEventArgs> OnGamepadChange;
        public event EventHandler<GamepadPressedEventArgs> OnGamepadPress;
        public event EventHandler<GamepadDownEventArgs> OnGamepadDown;
        public event EventHandler<GamepadReleasedEventArgs> OnGamepadRelease;

        public event EventHandler<MouseMoveEventArgs> OnMouseMove;

        // Events- can be called from within the class whenever needed.
        // Some of them are not complete yet, and still need custom eventargs.
        private void Initialised() { OnInitialise?.Invoke(this, EventArgs.Empty); }
        private void Updated() { OnUpdate?.Invoke(this, new UpdateEventArgs()); }
        private void PositionChanged()
        {
            OnPositionChange?.Invoke(this, EventArgs.Empty);

            if (Position.X < -(Sprite.Size.X + Sprite.Origin.X) ||
                Position.X > _game.Size.Width + Sprite.Origin.X ||
                Position.Y < -(Sprite.Size.Y + Sprite.Origin.Y) ||
                Position.Y > _game.Size.Height + Sprite.Origin.Y) { MovedOutsideScreen(); }
        }

        private void Activated() { _isActive = true; OnActivate?.Invoke(this, EventArgs.Empty); }
        private void Deactivated() { _isActive = false; OnDeactivate?.Invoke(this, EventArgs.Empty); }
        private void DirectionChanged() { OnDirectionChange?.Invoke(this, EventArgs.Empty); }
        private void SpeedChanged() { OnSpeedChange?.Invoke(this, EventArgs.Empty); }
        private void Destroyed() { OnDestroy?.Invoke(this, EventArgs.Empty); }
        private void BeforeDraw() { OnBeforeDraw?.Invoke(this, new DrawEventArgs(_game.SpriteBatch)); }
        private void AfterDraw() { OnAfterDraw?.Invoke(this, new DrawEventArgs(_game.SpriteBatch)); }
        private void MovedOutsideScreen() { OnOutsideScreen?.Invoke(this, EventArgs.Empty); }
        private void Collided(List<GameObject> instancesCollided) { OnCollision?.Invoke(this, new CollisionEventArgs(instancesCollided)); }

        private void KeyChanged(object sender, KeyboardChangedEventArgs e) { if (IsActive) { OnKeyChange?.Invoke(sender, e); } }
        private void KeyPressed(object sender, KeyboardPressedEventArgs e) { if (IsActive) { OnKeyPress?.Invoke(sender, e); } }
        private void KeyDowned(object sender, KeyboardDownEventArgs e) { if (IsActive) { OnKeyDown?.Invoke(sender, e); } }
        private void KeyReleased(object sender, KeyboardReleasedEventArgs e) { if (IsActive) { OnKeyRelease?.Invoke(sender, e); } }

        private void GamepadChanged(object sender, GamepadChangedEventArgs e) { if (IsActive) { OnGamepadChange?.Invoke(sender, e); } }
        private void GamepadPressed(object sender, GamepadPressedEventArgs e) { if (IsActive) { OnGamepadPress?.Invoke(sender, e); } }
        private void GamepadDowned(object sender, GamepadDownEventArgs e) { if (IsActive) { OnGamepadDown?.Invoke(sender, e); } }
        private void GamepadReleased(object sender, GamepadReleasedEventArgs e) { if (IsActive) { OnGamepadRelease?.Invoke(sender, e); } }

        private void MouseMoved(object sender, MouseMoveEventArgs e) { if (IsActive) { OnMouseMove?.Invoke(sender, e); } }



        /// <summary>
        /// Add a gameobject to a scene rather than to a game.
        /// </summary>
        /// <param name="scene"></param>
        public GameObject(Scene scene) : base(scene.GameMaster)
        {
            this.Scene = scene;
            scene.AddGameObject(this);

            Set(scene.GameMaster, null);
        }


        /// <summary>
        /// Constructor. Create a new instance within the game. Creating an instance adds it to the game's
        /// components automaticly.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        public GameObject(GameMaster game, Texture2D texture) : base(game)
        {
            Set(game, texture);
        }

        public GameObject(GameMaster game, Vector2? startPosition = null) : base(game)
        {
            Set(game, null);

            if (startPosition != null) { PositionStart = (Vector2)startPosition; }
        }

        // Code to be loaded after construction; may be overriden.
        public virtual void Load() { }

        private void Set(GameMaster game, Texture2D texture)
        {
            _game = game as GameMaster;
            _game.AddInstance(this);
            _spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));
            _sprite = new Sprite(texture);
            _isAlive = true;

            _resources = ResourceController.Initialize(_game);

            _input = _game.InputController;
            _input.KeyChanged += KeyChanged;
            _input.KeyPressed += KeyPressed;
            _input.KeyDown += KeyDowned;
            _input.KeyReleased += KeyReleased;

            _input.GamepadChanged += GamepadChanged;
            _input.GamepadPressed += GamepadPressed;
            _input.GamepadDown += GamepadDowned;
            _input.GamepadReleased += GamepadReleased;

            _input.MouseMoved += _input_MouseMoved;

            _position = new Vector2(_game.Size.Width / 2, _game.Size.Height / 2); // centerscreen.

            CheckCollisionsWith = new List<Type>();

            Load();
        }

        private void _input_MouseMoved(object sender, MouseMoveEventArgs e)
        {
            MouseMoved(sender, e);
        }

        private void _input_KeyChanged(object sender, KeyboardChangedEventArgs e)
        {
            KeyChanged(sender, e);
        }

        public override void Initialize()
        {
            base.Initialize();
            Initialised();
        }

        public sealed override void Update(GameTime gameTime)
        {
            if (!IsActive) { return; }

            base.Update(gameTime);

            DirectionVector += new Vector2(0, Gravity);
            Position += DirectionVector;

            Sprite?.Update();

            Updated();
        }

        public sealed override void Draw(GameTime gameTime)
        {
            if (!IsVisible) { return; }

            BeforeDraw();
            Sprite?.Draw(_spriteBatch, Position);
            AfterDraw();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Like destroy, but without fireing its Destroy event.
        /// </summary>
        public void ForceDestroy()
        {
            _isForceDestroyed = true;
            Destroy();
        }


        public void Destroy()
        {
            Destroyed();
            _isAlive = false;
        }

        /// <summary>
        /// Updates the boundingbox for an instance, with which the collisions will be checked.
        /// </summary>
        public void UpdateBoundingBox()
        {
            try
            {
                Sprite.BoundingBox.X = (int)(Position.X - Sprite.Origin.X);
                Sprite.BoundingBox.Y = (int)(Position.Y - Sprite.Origin.Y);
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); } // TODO: Sketchy code right here.
        }


        /// <summary>
        /// For each of the types in CheckCollisionsWith, check if the instance has a collision with any
        /// GameObject-instance that belongs to that type. If there was at least one collision, throw an event.
        /// </summary>
        public void CheckCollisions()
        {
            var instancesToCheck = GameMaster.GetInstancesOfTypes(CheckCollisionsWith);
            var instancesCollided = new List<GameObject>();

            foreach (var instance in instancesToCheck)
            {
                if (Sprite.BoundingBox.Intersects(instance.Sprite.BoundingBox)) { instancesCollided.Add(instance); }
            }

            if (instancesCollided.Count > 0)
            {
                Collided(instancesCollided);
            }
        }


        /// <summary>
        /// Calculates the position of an offset in regards to the position of the object, even if the object is rotated.
        /// Example: a bullet should come out of the parrel of a battleship, even if it's rotated.
        /// 
        /// TODO: This does not (yet?) take scaling of the object into consideration.
        /// </summary>
        /// <param name="offset">The offset that should be recalculated.</param>
        /// <returns>The new translated offset.</returns>
        public Vector2 CalculateRotatedOffset(Vector2 offset)
        {
            var length = offset.Length();
            var angle = Utility.VectorToAngle(offset) + Sprite.Rotation;
            var y = length * (float)Math.Sin(MathHelper.ToRadians(angle));
            var x = length * (float)Math.Cos(MathHelper.ToRadians(angle));
            var rotatedOffset = new Vector2(Position.X + x, Position.Y + y);

            return rotatedOffset;
        }

        /// <summary>
        /// Make sure that the object is fully within the screen; no part of it will be outside the screen after calling this function.
        /// 
        /// TODO: Scaling and rotation are NOT taken into account (yet?).
        /// </summary>
        public void PlaceWithinScreen()
        {
            var x = Position.X;
            var y = Position.Y;
            var height = Sprite.Size.Y; // TODO: calculate the rotated, scaled size of the sprite. What is the 
            var width = Sprite.Size.X; // smallest box that can be drawn around the sprite?

            if (Position.X < Sprite.Origin.X) { x = Sprite.Origin.X; }
            if (Position.Y < Sprite.Origin.Y) { y = Sprite.Origin.Y; }
            if (Position.X > _game.Size.Width - width + Sprite.Origin.X) { x = _game.Size.Width - width + Sprite.Origin.X; }
            if (Position.Y > _game.Size.Height - height + Sprite.Origin.Y) { y = _game.Size.Height - height + Sprite.Origin.Y; }

            Position = new Vector2(x, y);
        }

        /// <summary>
        /// Place the gameobject in the exact center of the screen.
        /// </summary>
        public void CenterScreen()
        {
            var height = Sprite.Size.Y;
            var width = Sprite.Size.X;
            var screenHeight = GameMaster.Size.Height;
            var screenWidth = GameMaster.Size.Width;
            var x = (screenWidth - width) / 2 + Sprite.Origin.X;
            var y = (screenHeight - height) / 2 + Sprite.Origin.Y;

            Position = new Vector2(x, y);
        }

        public void CenterOrigin()
        {
            var height = Sprite.Size.Y;
            var width = Sprite.Size.X;

            Sprite.Origin = new Vector2(width / 2, height / 2);
        }
    }







    /// <summary>
    /// All private variables- seems more clean this way, in my opinion (it's not :) ).
    /// </summary>
    public partial class GameObject
    {
        private Vector2 _position;
        private Vector2 _positionPrevious;
        private Vector2 _positionStart;
        private Vector2 _positionTarget;

        private Vector2 _directionVector;
        private float _directionDegrees;
        private float _speed;

        private bool _isAlive;
        private bool _isActive;
        private bool _isForceDestroyed = false;

        private SpriteBatch _spriteBatch;
        private Sprite _sprite;
        private GameMaster _game;

        private InputController _input;
        private ResourceController _resources;
    }

    public class CollisionEventArgs
    {
        public List<GameObject> Instances;

        public CollisionEventArgs(List<GameObject> instances)
        {
            Instances = instances;
        }
    }

    public class UpdateEventArgs
    {
        public GamePadThumbSticks Thumbsticks;
        public List<Buttons> PressedButtons;
        public List<Keys> PressedKeys;
        public Vector2 Cursor;

        public UpdateEventArgs()
        {
            var input = InputController.Instance;

            PressedKeys = input.GetPressedKeys();
            PressedButtons = input.GetPressedButtons();
            Cursor = input.GetCursor();
            Thumbsticks = input.GetThumbsticks();
        }
    }

    public class DrawEventArgs
    {
        public SpriteBatch SpriteBatch;

        public DrawEventArgs(SpriteBatch SpriteBatch)
        {
            this.SpriteBatch = SpriteBatch;
        }
    }

}

﻿using Master.Engine.Enums;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Entities
{
    public class Text : GameObject
    {
        public Text(Scene scene) : base(scene) { }

        public string Value {
            get { return _value; }
            set
            {
                _value = value;
                Sprite.Size = GameMaster.TextController.CalculateSize(_value, FontName);
            }
        }
        public string FontName { get; set; }

        private string _value;

        public override void Load()
        {
            Sprite = new Sprite();
            Sprite.Color = Color.Black;
            Sprite.DrawDepth = DrawDepths.Front;
            FontName = null;
            Value = "";

            OnAfterDraw += Text_OnAfterDraw;
        }

        private void Text_OnAfterDraw(object sender, DrawEventArgs e)
        {
            GameMaster.TextController.Write(Position, Value, Sprite, FontName);
        }
    }
}

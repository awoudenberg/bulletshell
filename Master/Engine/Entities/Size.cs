﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Entities
{
    public class Size
    {
        private int height;
        private int width;

        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public int Height
        {
            get { return height; }
            set { height = (value >= 0 ? value : 0); }
        }

        public int Width
        {
            get { return width; }
            set { width = (value >= 0 ? value : 0); }
        }
    }
}

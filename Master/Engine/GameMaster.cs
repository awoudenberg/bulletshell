﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System;
using System.Collections.Generic;

using MonogameEngine.Controllers;
using Master.Engine.Entities;
using Master.Engine.Controllers;
using Master.Engine;

namespace MonogameEngine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMaster : Game
    {
        protected GraphicsDeviceManager _graphics;
        protected SpriteBatch _spriteBatch;
        protected InputController _inputController;
        protected TextController _textController;
        protected ResourceController _resourceController;
        protected ScoreController _scoreController;
        protected SceneController _sceneController;
        protected SoundController _soundController;
        protected SettingController _settingController;

        public ResourceController ResourceController { get { return _resourceController; } }
        public InputController InputController { get { return _inputController; } }
        public TextController TextController { get { return _textController; } }
        public ScoreController ScoreController { get { return _scoreController; } }
        public SceneController SceneController { get { return _sceneController; } }
        public SoundController SoundController { get { return _soundController; } }
        public SettingController SettingController { get { return _settingController; } }

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
        }
        public Size Size
        {
            get
            {
                return new Size(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);
            }
            set
            {
                if (value == null || value.Height<=0 || value.Width<=0) { return; }

                // Set the size of the windows.
                _graphics.PreferredBackBufferWidth = value.Width;
                _graphics.PreferredBackBufferHeight = value.Height;
                _graphics.ApplyChanges();

                // Place it in the center of the screen.
                //var x = (GraphicsDevice.DisplayMode.Width - Size.Width) / 2;
                //var y = (GraphicsDevice.Viewport.Height - Size.Height) / 2;
                //Window.Position = new Point(x, y);
            }
        }

        private List<GameComponent> _componentsToAdd;

        protected Object _criticalCodeLock;

        public GameMaster()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _componentsToAdd = new List<GameComponent>();
            _criticalCodeLock = new Object();

            Size = new Size(1280, 720);
        }




        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _resourceController = ResourceController.Initialize(this);
            _sceneController = SceneController.Initialize(this);
            _soundController = SoundController.Initialize(this);
            _inputController = InputController.Initialize(this);
            _scoreController = ScoreController.Initialize(this);
            _textController = TextController.Initialize(this);
            _settingController = SettingController.Initialize(this);

            Utility.Game = this;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), _spriteBatch); // is this really needed?
            _textController.SpriteBatch = _spriteBatch;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            RemoveAllInstances();
        }

        private void RemoveAllInstances()
        {
            lock (_criticalCodeLock)
            {
                for (int i = Components.Count - 1; i >= 0; i--)
                {
                    Components.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            AddInstancesToComponents();
            RemoveDeadInstances();
            CheckCollisions(GetAllInstances());

            base.Update(gameTime);
        }

        /// <summary>
        /// No longer allow instances to add themselves directly to the Componentslist; only do so before performing update().
        /// </summary>
        private void AddInstancesToComponents()
        {
            if (_componentsToAdd?.Count > 0)
            {
                lock (_criticalCodeLock)
                {
                    _componentsToAdd.ForEach(x => Components.Add(x));
                    _componentsToAdd.Clear();
                }
            }
        }

        /// <summary>
        /// TODO: Don't run this every update, but every n seconds?
        /// </summary>
        private void RemoveDeadInstances()
        {
            lock (_criticalCodeLock)
            {
                for (int i = Components.Count - 1; i >= 0; i--)
                {
                    if (Components[i] is GameObject && !(Components[i] as GameObject).IsAlive)
                    {
                        var gameObject = Components[i] as GameObject;

                        gameObject.Scene?.RemoveGameObject(gameObject); // needed, else it remains in memory.
                        Components.RemoveAt(i);
                    }
                }
            }
        }

        /// <summary>
        /// Get all gameobjects from the componentslist.
        /// 
        /// TODO: Review if this is needed, I think we only add gameobjects anyways.
        /// TODO: Definitely needed, but objects are added and removed at the wrong time, during this function- that needs a major review!!!
        /// 08/11/2016: Added a lock. Collisions (now?) don't seem to be working...
        /// </summary>
        /// <returns></returns>
        public List<GameObject> GetAllInstances()
        {
            lock (_criticalCodeLock)
            {
                var instances = new List<GameObject>();
                var componentEnum = Components.ToList().GetEnumerator(); // tolist needed to fix a bug. - BUG: This Still needs to be fixed :)

                while (componentEnum.MoveNext())
                {
                    if (componentEnum.Current is GameObject)
                    {
                        instances.Add((GameObject)componentEnum.Current);
                    }
                }

                return instances;
            }
        }


        /// <summary>
        /// Check all collisions between the tracked instances.
        /// </summary>
        /// <param name="instances"></param>
        private void CheckCollisions(List<GameObject> instances)
        {
            lock (_criticalCodeLock)
            {
                instances.ForEach(x => x.UpdateBoundingBox()); // first make sure all boundingboxes are correct...
                instances.ForEach(x => x.CheckCollisions()); // ...then check collisions with other noteworthy instances.
            }
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            lock (_criticalCodeLock)
            {
                // New: should enforce ordering by DrawOrder.
                _spriteBatch.Begin(SpriteSortMode.BackToFront, null);
                base.Draw(gameTime); // moet tussen begin en eind staan!
                _spriteBatch.End();
            }
        }

        /// <summary>
        /// Remove an instance from the game. Gets called when an instance gets destroyed.
        /// </summary>
        /// <param name="instance"></param>
        public void RemoveInstance(GameComponent instance)
        {
            // BUG: I think this is causing problems. Best let gamemaster decide when to remove them!
            //Components?.Remove(instance); 
        }

        /// <summary>
        /// Add an instance to the game. Gets called when an instance gets created.
        /// 
        /// TODO: Review this code- directly writing to the components seems dangerous! Maybe make a buffer, and move from buffer to
        /// Components when in update?
        /// TODO: Or take a look at thread-safe collections!!!!
        /// </summary>
        /// <param name="instance"></param>
        public void AddInstance(GameComponent instance)
        {
            //Components?.Add(instance);
            lock (_criticalCodeLock)
            {
                _componentsToAdd?.Add(instance);
            }
        }

        /// <summary>
        /// Utility. Get all instances that have a type that exists in the list of types we want to check.
        /// 
        /// TODO: Werkt niet helemaal wanneer het object een kind is van de klasse waarmee gecontroleerd moet worden!
        /// </summary>
        /// <param name="checkCollisionsWith"></param>
        /// <returns></returns>
        public List<GameObject> GetInstancesOfTypes(List<Type> checkCollisionsWith)
        {
            lock (_criticalCodeLock)
            {
                return (checkCollisionsWith == null ? new List<GameObject>() : (from x in GetAllInstances() where checkCollisionsWith.Contains(x.GetType()) select x).ToList());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Enums
{
    public static class DrawDepths
    {
        public static float Front = 0.0001f;
        public static float Default = 0.5f;
        public static float Back = 0.9999f;
    }
}

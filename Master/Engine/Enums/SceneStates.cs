﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Enums
{
    /// <summary>
    /// States that a scene can have.
    /// </summary>
    public enum SceneStates
    {
        InvisibleInactive,
        InvisibleActive,
        VisibleInactive,
        VisibleActive,
    }
}

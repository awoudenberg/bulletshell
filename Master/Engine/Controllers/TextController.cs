﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonogameEngine.Controllers
{
    /// <summary>
    /// First attempt at a TextController. Needs a lot of work still :)
    /// 
    /// Author: Anthony Woudenberg, anthony.woudenberg@gmail.com
    /// </summary>
    public sealed class TextController : DrawableGameComponent
    {
        // Singleton requirements. By doing this, we can have only one TextController in the game, even it is is
        // 'created' on several places.
        private static TextController _instance;
        private static GameMaster _game;

        private TextController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);
            _spriteBatch = _game.SpriteBatch;

            _fonts = new Dictionary<string, SpriteFont>();
            _fonts.Add("default", ResourceController.Instance.LoadResource<SpriteFont>("default"));
            _fonts.Add("hiscores", ResourceController.Instance.LoadResource<SpriteFont>("hiscores"));
            _fonts.Add("levelTitle", ResourceController.Instance.LoadResource<SpriteFont>("levelTitle"));
            _fonts.Add("levelSubtitle", ResourceController.Instance.LoadResource<SpriteFont>("levelSubtitle"));
        }
        public static TextController Instance
        {
            get
            {
                if (_instance == null && _game != null)
                {
                    _instance = new TextController(_game);
                }
                return _instance;
            }
        }
        public static TextController Initialize(GameMaster game)
        {
            _game = game;

            return Instance;
        }

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
            set { _spriteBatch = value; }
        }
        // Members
        private Dictionary<string, SpriteFont> _fonts;
        private SpriteBatch _spriteBatch;


        public void Write(Vector2 position, string text, Color? color = null, string fontName = null)
        {
            var font = (fontName == null ? _fonts.First().Value : _fonts.ContainsKey(fontName) ? _fonts[fontName] : _fonts.First().Value);
            var fontColor = (color == null ? Color.Black : (Color)color);

            _spriteBatch?.DrawString(font, text, position, fontColor);
        }

        public void Write(Vector2 position, string text, Sprite sprite, string fontName = null)
        {
            var font = (fontName == null ? _fonts.First().Value : _fonts.ContainsKey(fontName) ? _fonts[fontName] : _fonts.First().Value);

            _spriteBatch?.DrawString(font, text, position, sprite.Color, sprite.Rotation, sprite.Origin, sprite.Scale, sprite.SpriteEffects, sprite.DrawDepth);
        }


        public Vector2 CalculateSize(string text, string fontName = null)
        {
            var font = (fontName == null ? _fonts.First().Value : _fonts.ContainsKey(fontName) ? _fonts[fontName] : _fonts.First().Value);
            var lineSize = font.MeasureString(text);

            return lineSize;
        }
    }
}

﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Controllers
{
    public sealed class SceneController : DrawableGameComponent
    {
        // Singleton requirements. By doing this, we can have only one inputcontroller in the game, even it is is
        // 'created' on several places.
        private static SceneController _instance;
        private static GameMaster _game;

        /// <summary>
        /// Private constructor, used for singleton-pattern.
        /// </summary>
        /// <param name="game"></param>
        private SceneController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);
        }

        /// <summary>
        /// Get an instance of the SceneController, creating one if there wasn't one created earlier.
        /// </summary>
        public static SceneController Instance
        {
            get
            {
                if (_instance == null && _game != null) { _instance = new SceneController(_game); }

                return _instance;
            }
        }

      

        /// <summary>
        /// Initialize the controller.
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public static SceneController Initialize(GameMaster game)
        {
            _game = game;

            return Instance;
        }



        // Members
        private Dictionary<string, Scene> _scenes;


        // Functions
        public void AddScene(string name, Scene scene)
        {
            if (_scenes == null) { _scenes = new Dictionary<string, Scene>(); }

            if (_scenes.ContainsKey(name))
            {
                _scenes[name] = scene;
            }
            else
            {
                _scenes.Add(name, scene);
            }
        }

        /// <summary>
        /// Inactivate all other scenes, and hide them. Activate the wanted scene.
        /// </summary>
        /// <param name="name"></param>
        public void ActivateSceneSolo(string name)
        {
            if (!_scenes.ContainsKey(name)) { return; }

            foreach (KeyValuePair<string, Scene> scene in _scenes)
            {
                scene.Value.IsSolo = false;
                scene.Value.SetState(Enums.SceneStates.InvisibleInactive);
            }

            // Resize the window if the scene has a size.
            _game.Size = _scenes[name].Size;

            _scenes[name].IsSolo = true;
            _scenes[name].SetState(Enums.SceneStates.VisibleActive);
        }

        /// <summary>
        /// Inactivate all other scenes, but keep their current visibility. Activate the wanted scene.
        /// </summary>
        /// <param name="name"></param>
        public void ActivateScene(string name)
        {
            if (!_scenes.ContainsKey(name)) { return; }

            foreach (KeyValuePair<string, Scene> scene in _scenes)
            {
                scene.Value.IsSolo = false;
                scene.Value.IsActive = false;
            }
            _scenes[name].IsSolo = false;
            _scenes[name].SetState(Enums.SceneStates.VisibleActive);
        }


        public void Pause(string name)
        {
            if (!_scenes.ContainsKey(name)) { return; }

            _scenes[name].IsActive = !_scenes[name].IsActive;
        }

        public void Unpause(string name)
        {
            if (_scenes.ContainsKey(name)) {
                _scenes[name].IsPaused = false;
            }
        }

        public Scene GetScene(string name)
        {
            return (_scenes.ContainsKey(name)?_scenes[name]:null);
        }
    }


}

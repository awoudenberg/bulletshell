﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Controllers
{
    public sealed class SoundController : DrawableGameComponent
    {

        #region Singleton requirements
        private static SoundController _instance;
        private static GameMaster _game;

        /// <summary>
        /// Constructor. Private because of the singleton pattern.
        /// </summary>
        /// <param name="game"></param>
        private SoundController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);

            _masterPan = 0.0f;
            _masterPitch = 0.0f;
            _masterVolume = 1.0f;
            _volumeSoundEffect = 1.0f;
            _volumeMusic = 1.0f;

            UseMusic = true;
            UseEffects = true;

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = _masterVolume * _volumeMusic;
            SoundEffect.MasterVolume = _masterVolume * _volumeSoundEffect;
        }

        /// <summary>
        /// Ask for an instance of the controller. If it didn't exist, it will create one. The controller is
        /// guaranteed to be unique throughout the entire game.
        /// </summary>
        public static SoundController Instance
        {
            get
            {
                if (_instance == null && _game != null) { _instance = new SoundController(_game); }

                return _instance;
            }
        }

        /// <summary>
        /// Set the game, and instantiate the controller.
        /// </summary>
        public static SoundController Initialize(GameMaster game)
        {
            _game = _game ?? game;

            return Instance;
        }
        #endregion

        #region Members
        public bool UseMusic
        {
            get { return _useMusic; }
            set
            {
                _useMusic = value;

                if (!_useMusic) { _music?.Stop(); } else { _music?.Play(); }
            }
        }
        public bool UseEffects { get; set; }
        public float MasterVolume
        {
            get { return _masterVolume; }
            set
            {
                _masterVolume = Utility.Limit(value, 0.0f, 1.0f);
                SoundEffect.MasterVolume = _masterVolume;
            }
        }
        public float VolumeSoundEffect
        {
            get { return _volumeSoundEffect; }
            set
            {
                _volumeSoundEffect = Utility.Limit(_masterVolume * value, 0.0f, 1.0f);
                SoundEffect.MasterVolume = _volumeSoundEffect;
            }
        }
        public float VolumeMusic
        {
            get { return _volumeMusic; }
            set
            {
                _volumeMusic = Utility.Limit(_masterVolume * value, 0.0f, 1.0f);
                MediaPlayer.Volume = _volumeMusic;
            }
        }
        public float MasterPitch
        {
            get { return _masterPitch; }
            set { _masterPitch = Utility.Limit(value, -1.0f, 1.0f); }
        }
        public float MasterPan
        {
            get { return _masterPan; }
            set { _masterPan = Utility.Limit(value, -1.0f, 1.0f); }
        }

        private SoundEffectInstance _music;
        private string _musicName;
        private float _volumeSoundEffect;
        private float _masterVolume;
        private float _volumeMusic;
        private float _masterPitch;
        private float _masterPan;
        private bool _useMusic;

        #endregion

        #region  Methods

        public void Loop(SoundEffect song)
        {

            // Hmmm... this apparently won't work... trying Mr. Vangeel's method instead.
            //MediaPlayer.IsRepeating = true;
            //MediaPlayer.Stop();
            //MediaPlayer.Volume = VolumeMusic;
            //MediaPlayer.Play(song);


            // ...which works :)
            if (_musicName != song.Name)
            {
                _musicName = song.Name;
                _music?.Stop();
                _music = song.CreateInstance();
                _music.IsLooped = true;

                if (UseMusic) { _music.Play(); }
            }
        }

        public void Loop(string songName)
        {
            var song = _game.ResourceController.LoadResource<SoundEffect>("Music/" + songName);

            Loop(song);
        }



        /// <summary>
        /// Plays a soundeffect
        /// </summary>
        public void Play(SoundEffect soundEffect, float pan = 0.0f, float volume = 1.0f, float pitch = 0.0f)
        {
            if (!UseEffects) { return; }

            volume = Utility.Limit(volume, 0.0f, 1.0f);
            pitch = Utility.Limit(_masterPitch + pitch, -1.0f, 1.0f);
            pan = Utility.Limit(_masterPan + pan, -1.0f, 1.0f);

            soundEffect.Play(volume, pitch, pan);
        }

        /// <summary>
        /// Plays a soundeffect
        /// </summary>
        public void Play(string soundEffectName, float pan = 0.0f, float volume = 1.0f, float pitch = 0.0f)
        {
            var soundEffect = _game.ResourceController.LoadResource<SoundEffect>("soundeffects/" + soundEffectName);

            Play(soundEffect, pan, volume, pitch);
        }


        #endregion

    }



}

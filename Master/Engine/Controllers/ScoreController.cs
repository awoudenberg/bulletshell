﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine.Entities;
using Master.Engine.Entities;
using System.Xml.Serialization;
using System.IO;
using Master.Engine;

namespace MonogameEngine.Controllers
{
    public sealed class ScoreController : DrawableGameComponent
    {
        #region Singleton requirements
        private static ScoreController _instance;
        private static GameMaster _game;
        private ScoreController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);
            _spriteBatch = _game.SpriteBatch;
            _textController = TextController.Instance;

            _fonts = new Dictionary<string, SpriteFont>();
            _fonts.Add("Default", ResourceController.Instance.LoadResource<SpriteFont>("Default"));

            LoadHiscores();
        }


        public static ScoreController Instance
        {
            get
            {
                if (_instance == null && _game != null)
                {
                    _instance = new ScoreController(_game);
                }
                return _instance;
            }
        }



        public static ScoreController Initialize(GameMaster game)
        {
            _game = game;

            return Instance;
        }
        public Vector2 Position;
        #endregion

        #region Members
        private const int MAX_NUMBER_OF_HISCORES = 10;

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
            set { _spriteBatch = value; }
        }

        private Dictionary<string, SpriteFont> _fonts;
        private TextController _textController;
        private List<Hiscore> _hiscores;
        private SpriteBatch _spriteBatch;
        private Points _score;
        private Scene _scene;
        #endregion

        #region Methods
       

        public void AwardPoints(Vector2 position, int value, Color? color = null, string fontName = null)
        {
            if (_scene == null) { return; }

            var font = (fontName == null ? _fonts.First().Value : _fonts.ContainsKey(fontName) ? _fonts[fontName] : _fonts.First().Value);
            var fontColor = (color == null ? Color.Black : (Color)color);
            var points = new Points(_scene);

            _score.Value += value;

            points.Position = position;
            points.Value = value;
            points.DirectionDegrees = 90;
            points.Speed = 0.5f;
        }

        public void Reset()
        {
            _score.Value = 0;
        }


        /// <summary>
        /// Set a scene to display the score on.
        /// </summary>
        public void SetScene(Scene scene)
        {
            _scene = scene;

            _score = new Points(scene);
            _score.DestroyAfterInterval = false;
            _score.Position = new Vector2(10, 10);
            _score.Value = 0;
        }




        public List<Hiscore> LoadHiscores()
        {
            try
            {

                var path = @"hiscores.xml";
                var serializer = new XmlSerializer(typeof(List<Hiscore>));
                var reader = new StreamReader(path);

                _hiscores = ((List<Hiscore>)serializer
                    .Deserialize(reader))
                    .OrderByDescending(o => o.Score)
                    .ThenByDescending(o => o.Date)
                    .Take(MAX_NUMBER_OF_HISCORES).ToList();

                reader.Close();
            }
            catch (Exception)
            {
                _hiscores = new List<Hiscore>();
            }

            return _hiscores;
        }

        /// <summary>
        /// Add a hiscore if it's high enough. Return the index of the newly added score, if it was added; else, return -1.
        /// </summary>
        public int AddHiscore(string name = "")
        {
            var hiscore = new Hiscore { Date = DateTime.Now, Score = _score.Value, Name = (name==""?Environment.UserName:name) };

            LoadHiscores();

            _hiscores.Add(hiscore);
            _hiscores = _hiscores
                .OrderByDescending(o => o.Score)
                .ThenByDescending(o => o.Date)
                .Take(MAX_NUMBER_OF_HISCORES).ToList();

            StoreHiscores();

            return (_hiscores.Contains(hiscore) ? _hiscores.FindIndex(h => h.Date == hiscore.Date) : -1);
        }

        private void StoreHiscores()
        {
            Utility.SaveToXML<List<Hiscore>>(_hiscores, @"hiscores.xml");
        }

        public void AlterName(int index, string name)
        {
            if (index >= 0 && index < _hiscores.Count)
            {
                _hiscores[index].Name = name;
                StoreHiscores();
            }
        }


        #endregion
    }
}

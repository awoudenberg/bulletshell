﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonogameEngine.Controllers
{


    public sealed class ResourceController : DrawableGameComponent
    {
        // Singleton requirements. By doing this, we can have only one ResourceController in the game, even it is is
        // 'created' on several places.
        private static ResourceController _instance;
        private static GameMaster _game;
        private static Texture2D _emptyTexture;

        private ResourceController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);

            _emptyTexture = GetEmptyTexture();
        }
        public static ResourceController Instance
        {
            get
            {
                if (_instance == null && _game != null)
                {
                    _instance = new ResourceController(_game);
                }
                return _instance;
            }
        }
        public static ResourceController Initialize(GameMaster game)
        {
            _game = _game??game;

            return Instance;
        }

        // Resourcelists
        Dictionary<string, object> Content = new Dictionary<string, object>();

        /// <summary>
        /// Loads a resource, and adds it to the dictionairy. If there is already something there with the same name, it gets
        /// returned rather than being loaded again.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T LoadResource<T>(string name)
        {
            if(Content.ContainsKey(name)) { return (T)Content[name]; }

            // try/catch implemented because of multithreading
            try
            {
                var resource = _game.Content.Load<T>(name);

                Content.Add(name, resource);

                return resource;
            } catch(Exception)
            {
                return (T)Content[name]; // TODO: Bug here, upon exiting without loading the resource first!
            }
        }

        internal Texture2D GetEmptyTexture(int width=1, int height=1)
        {
            // optimalisation- most are 1x1 anyways, and is therefore cached..
            if (width==1 && height==1 && _emptyTexture!=null) {return _emptyTexture; } 

            var emptyTexture = new Texture2D(GraphicsDevice, width, height);
            emptyTexture.SetData(new[] { Color.White });

            return emptyTexture;
        }
    }


   
}

﻿using Master.Engine.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonogameEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Engine.Controllers
{
    public sealed class SettingController : DrawableGameComponent
    {

        #region Singleton requirements
        private static SettingController _instance;
        private static GameMaster _game;

        private SettingController(GameMaster game) : base(game)
        {
            _game = game;
            _game.AddInstance(this);
            _spriteBatch = _game.SpriteBatch;

            LoadSettings();
        }


        public static SettingController Instance
        {
            get
            {
                if (_instance == null && _game != null)
                {
                    _instance = new SettingController(_game);
                }
                return _instance;
            }
        }



        public static SettingController Initialize(GameMaster game)
        {
            _game = game;

            return Instance;
        }
        #endregion

        #region Members

        private const string _fileSettings = "settings.xml";

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
            set { _spriteBatch = value; }
        }
        public Settings Settings
        {
            get { return _settings; }
        }

        private SpriteBatch _spriteBatch;
        private Settings _settings;

        #endregion

        #region Methods
     

        public void LoadSettings()
        {
            _settings = Utility.LoadFromXML<Settings>(_fileSettings);

            LimitSettings();
            EnforceSettings();
        }

        private void LimitSettings()
        {
            _settings.MasterVolume = MathHelper.Clamp(_settings.MasterVolume, 0.0f, 1.0f);
            _settings.AmountOfBubblesBoat = (int)MathHelper.Clamp(_settings.AmountOfBubblesBoat, 0.0f, 3.0f);
            _settings.AmountOfBubblesEnemy = (int)MathHelper.Clamp(_settings.AmountOfBubblesEnemy, 0.0f, 20.0f);

        }

        public void EnforceSettings()
        {
            _game.SoundController.MasterVolume = MathHelper.Clamp(_settings.MasterVolume, 0.0f, 1.0f); // Just in case :)
            _game.SoundController.UseMusic = _settings.UseMusic;
            _game.SoundController.UseEffects = _settings.UseSoundEffects;
        }
      

        public void StoreSettings()
        {
            LimitSettings();

            Utility.SaveToXML(_settings, _fileSettings);
        }

        public void PersistSettings()
        {
            StoreSettings();
            LoadSettings();
        }     

        #endregion
    }


}

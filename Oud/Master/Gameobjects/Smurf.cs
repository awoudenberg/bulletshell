﻿using Master.Engine;
using Master.Gameobjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    public enum SmurfStates
    {
        standing, walking, jumping
    }

    // TODO: Move to Engine.
    public static class Directions
    {
        public const float Right = 0.0f;
        public const float Up = 90.0f;
        public const float Left = 180.0f;
        public const float Down = 270.0f;
    }

    public class Smurf : GameObject
    {
        public int WalkingSpeed; // the amount of pixels the smurf moves per step.
        public int BoundaryRight; // the smurf stops moving when encountering this (right) border
        public SmurfStates State;

        private float _faceDirection = Directions.Right;
        private float _jumpingPower = 15.0f;
        private bool _isScrolling = false;


        /// <summary>
        /// Constructor. Create a smurf that can walk between screenleft and centerscreen.
        /// </summary>
        /// <param name="game"></param>
        public Smurf(GameMaster game, Vector2? startPosition) : base(game, startPosition)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Smurf"), 4, 4, 5);
            Sprite.Animate = false;
            Sprite.CurrentFrame = 3;
            Sprite.Origin = new Vector2(0, Sprite.Size.Y); // bottomleft, rather than deadcenter.
            Sprite.DrawBoundingBox = true;

            BoundaryRight = (game.Size.Width - (int)Sprite.Size.X) / 2;
            State = SmurfStates.standing;
            Gravity = 0.5f;
            WalkingSpeed = 4;

            OnPositionChange += Smurf_OnPositionChange;
            OnCollision += Smurf_OnCollision;
            OnKeyPress += Smurf_OnKeyPress;
            OnKeyDown += Smurf_KeyDown;
            OnUpdate += Smurf_OnUpdate;
            OnAfterDraw += Smurf_OnAfterDraw;

            CheckCollisionsWith.Add(typeof(Chicken));
            CheckCollisionsWith.Add(typeof(Tree));
        }

        private void Smurf_OnAfterDraw(object sender, DrawEventArgs e)
        {
            TextController.Instance.Write(Position-Sprite.Origin, "test");
        }

        private void Smurf_OnCollision(object sender, CollisionEventArgs e)
        {
            foreach (var instance in e.Instances)
            {
                if (instance.GetType() == typeof(Tree) &&
                    (Position.Y > instance.Position.Y && PositionPrevious.Y <= instance.Position.Y))
                {
                    instance.Sprite.DrawBoundingBox = true;
                    State = (DirectionVector.X == 0 ? SmurfStates.standing : SmurfStates.walking);
                    DirectionVector = new Vector2(DirectionVector.X, 0);
                    Position = new Vector2(Position.X, instance.Position.Y-Gravity); // -Gravity zorgt voor stabiliteit.
                }

                if (instance.GetType() == typeof(Chicken))
                {
                    if (Position.Y < instance.Position.Y && DirectionVector.Y > 0) // only when jumping on it
                    {
                        instance.Destroy();
                    }
                    else
                    {
                        Sprite.Color = Color.Pink;
                    }
                }

            }
        }

        private void Smurf_OnUpdate(object sender, UpdateEventArgs e)
        {
            if (Position.X < 0) { Position = new Vector2(0, Position.Y); }
            if (Position.X > BoundaryRight) { Position = new Vector2(BoundaryRight, Position.Y); }

            Sprite.Animate = (State == SmurfStates.walking);
            Sprite.SpriteEffects = (_faceDirection == Directions.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);

            if (State == SmurfStates.walking && !e.PressedKeys.Contains(Keys.Left) && !e.PressedKeys.Contains(Keys.Right))
            {
                Speed = 0;
                State = SmurfStates.standing;
                Sprite.CurrentFrame = 3;
            }

            // Very tricky- it should only move when the player is deadcenter of the screen, and then only if he's moving right
            // OR is stlil in the middle of a forward jump. Stationairy jumps should not cause scrolling.
            _isScrolling = (Position.X >= BoundaryRight && (e.PressedKeys.Contains(Keys.Right) ||
                (State == SmurfStates.jumping && DirectionVector.X > 0)));
        }

        private void Smurf_OnPositionChange(object sender, EventArgs e)
        {
            if (Position.Y >= PositionStart.Y) // "solid ground"
            {
                State = (DirectionVector.X == 0 ? SmurfStates.standing : SmurfStates.walking);
                DirectionVector = new Vector2(DirectionVector.X, 0);
                Position = new Vector2(Position.X, PositionStart.Y);
            }
        }


        private void Smurf_OnKeyPress(object sender, KeyboardPressedEventArgs e)
        {
            if (e.Keys.Contains(Keys.Space) && State != SmurfStates.jumping)
            {
                State = SmurfStates.jumping;
                DirectionVector -= new Vector2(0, _jumpingPower);
            }
        }


        /// <summary>
        /// Move the smurf is we press left or right, and animate him.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Smurf_KeyDown(object sender, KeyboardDownEventArgs e)
        {
            // Only allow changes in movement when the smurf is not already airborne.
            if (State != SmurfStates.jumping)
            {
                if (e.Keys.Contains(Keys.Right) || e.Keys.Contains(Keys.Left))
                {
                    DirectionDegrees = (e.Keys.Contains(Keys.Right) ? Directions.Right : Directions.Left);

                    State = SmurfStates.walking;
                    Speed = WalkingSpeed;
                    _faceDirection = DirectionDegrees;
                }
            }
        }

        /// <summary>
        /// Is the player in a position, speed and direction where it causes other objects to scroll? If not, this
        /// function returns 0, else it returns the speed with which other objects should scroll.
        /// </summary>
        /// <returns></returns>
        public int IsScrolling()
        {
            return (_isScrolling ? WalkingSpeed : 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Master
{
    /// <summary>
    /// Simple gameobject that reacts to a variety of events, to display some of the 
    /// functionality of the game engine.
    /// 
    /// Author: Anthony Woudenberg, anthony.woudenberg@gmail.com
    /// </summary>
    class Ball : GameObject
    {

        public Ball(GameMaster game) : base(game)
        {
            Sprite.Set(Resources.LoadResource<Texture2D>("ball"));

            OnKeyChange += Ball_KeyChanged;
            OnKeyDown += Ball_KeyDown;
            OnKeyRelease += Ball_KeyReleased;
        }


        private void Ball_KeyReleased(object sender, KeyboardReleasedEventArgs e)
        {
            if (e.KeysReleased.Contains(Keys.X)) { Destroy(); }
            if (e.KeysReleased.Contains(Keys.C)) { Sprite.Color = (Sprite.Color == Color.White ? Color.Gray : Color.White); }
            if (e.KeysReleased.Contains(Keys.Down)) { Speed--; }
            if (e.KeysReleased.Contains(Keys.Up)) { Speed++; }
        }

        private void Ball_KeyDown(object sender, KeyboardDownEventArgs e)
        {
            if (e.Keys.Contains(Keys.Left)) { DirectionDegrees += 5; Sprite.Rotation -= 5; }
            if (e.Keys.Contains(Keys.Right)) { DirectionDegrees -= 5; Sprite.Rotation += 5; }
            if (e.Keys.Contains(Keys.R)) { Sprite.Rotation += 5; }
            if (e.Keys.Contains(Keys.S)) { Sprite.Scale += new Vector2(0.01f, 0.01f); }
            if (e.Keys.Contains(Keys.W)) { Sprite.Scale -= new Vector2(0.01f, 0.01f); }
        }


        private void Ball_KeyChanged(object sender, KeyboardChangedEventArgs e)
        {
            if (e.Keys.Contains(Keys.RightControl)) { Position += new Vector2(50, 0); }
        }




    }
}

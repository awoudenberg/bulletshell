﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Master.Gameobjects
{
    public class ChickenManager : GameObject
    {
        public Smurf Player;

        Random _rng;
        Timer _timer;

        public ChickenManager(GameMaster game) : base(game)
        {
            _timer = new Timer();
            _rng = new Random();

            _timer.Interval = 2000;
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            SpawnChicken();
        }

        private void SpawnChicken()
        {
            var chicken = new Chicken(GameMaster, this);

            chicken.Position = new Vector2(GameMaster.Size.Width, Position.Y);
            chicken.Speed = _rng.Next(1, 5);
        }
    }
}

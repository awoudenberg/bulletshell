﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Master.Gameobjects
{
    public class TreeManager : GameObject
    {
        public Smurf Player;
        List<Tree> _trees;

        public TreeManager(GameMaster game) : base(game)
        {
            _trees = new List<Tree>();

            OnUpdate += TreeManager_OnUpdate;
            CreateTrees();
        }

        private void TreeManager_OnUpdate(object sender, UpdateEventArgs e)
        {
            _trees.ForEach(x => x.Position -= new Vector2(Player.IsScrolling(), 0));
        }

        private void CreateTrees()
        {
            for (int i = 4; i < 9; i++) { _trees.Add(new Tree(GameMaster, new Vector2(i * 50, GameMaster.Size.Height - 137))); }
            for (int i = 10; i < 13; i++) { _trees.Add(new Tree(GameMaster, new Vector2(i * 50, GameMaster.Size.Height - 187))); }
            for (int i = 15; i < 20; i++) { _trees.Add(new Tree(GameMaster, new Vector2(i * 50, GameMaster.Size.Height - 237))); }
        }
    }
}

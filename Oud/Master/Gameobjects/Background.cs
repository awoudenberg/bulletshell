﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Gameobjects
{
    class Background : GameObject
    {
        public Smurf Player;

        public Background(GameMaster game) : base(game)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("background"));
            Sprite.Origin = new Vector2(0, 0);
            Position = new Vector2(0, 0);
            
            OnPositionChange += Background_OnPositionChange;
            OnUpdate += Background_OnUpdate;
        }

        private void Background_OnUpdate(object sender, UpdateEventArgs e)
        {
            Position -= new Vector2(Player.IsScrolling(), 0);
        }

        /// <summary>
        /// Check if, when moving, we need to alter the position of the background to ensure a smooth, infinite background.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Background_OnPositionChange(object sender, EventArgs e)
        {
            if (Position.X > 0) { Position = new Vector2(GameMaster.Size.Width - Sprite.Size.X, 0); }
            if (Position.X < -Sprite.Size.X + GameMaster.Size.Width) { Position = new Vector2(0, 0); }
        }

    }
}

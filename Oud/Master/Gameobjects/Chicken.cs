﻿using Master.Gameobjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    class Chicken : GameObject
    {
        public Smurf Player;

        ChickenManager _manager;
        SoundEffect _sndChicken;

        public Chicken(GameMaster game, ChickenManager manager) : base(game)
        {
            _manager = manager;
            Player = manager.Player;

            _sndChicken = (SoundEffect)Resources.LoadResource<SoundEffect>("sndChicken");
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("ChickenRight"), 6, 5, 5);
            Sprite.Animate = true;
            Sprite.SpriteEffects = SpriteEffects.FlipHorizontally;
            Sprite.Origin = new Vector2(0, Sprite.Size.Y);

            DirectionDegrees = Directions.Left;

            OnSpeedChange += Chicken_OnSpeedChange;
            OnOutsideScreen += Chicken_OnOutsideScreen;
            OnUpdate += Chicken_OnUpdate;
            OnDestroy += Chicken_OnDestroy;
        }

        

        private void Chicken_OnDestroy(object sender, EventArgs e)
        {
            if (Position.X > 0) { _sndChicken.Play(); }
        }

        private void Chicken_OnUpdate(object sender, UpdateEventArgs e)
        {
            Position -= new Vector2(Player.IsScrolling(), 0);
        }    


        private void Chicken_OnOutsideScreen(object sender, EventArgs e)
        {
            if (Position.X < 0) { Destroy(); }
        }

        private void Chicken_OnSpeedChange(object sender, EventArgs e)
        {
            Sprite.Speed = (int)(5 / Speed); // adjust the animationspeed.
        }
    }
}

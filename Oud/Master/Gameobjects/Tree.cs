﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Gameobjects
{
    class Tree : GameObject
    {
        public Tree(GameMaster game, Vector2? startPosition) : base(game, startPosition)
        {
            Sprite = new Sprite(Resources.LoadResource<Texture2D>("Tree"));
            Sprite.Origin = new Vector2(0, 0);
        }
    }
}

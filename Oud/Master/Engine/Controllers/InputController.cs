﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Master
{
    /// <summary>
    /// First attempt at an inputcontroller. Keyboard has been tested, gamepad is yet untested, and mouse
    /// still has to be implemented.
    /// 
    /// Author: Anthony Woudenberg, anthony.woudenberg@gmail.com
    /// </summary>
    public sealed class InputController : DrawableGameComponent
    {
        // Singleton requirements. By doing this, we can have only one inputcontroller in the game, even it is is
        // 'created' on several places.
        private static InputController _instance;
        private static GameMaster _game;
        private InputController(GameMaster game) : base(game) {
            _keyboardCurrent = Keyboard.GetState();
            _game = game;
            _game.AddInstance(this);
        }
        public static InputController Instance
        {
            get
            {
                if (_instance == null && _game!=null)
                {
                    _instance = new InputController(_game);
                }
                return _instance;
            }
        }
        public static InputController Initialize(GameMaster game)
        {
            _game = game;

            return Instance;
        }



        // Members
        private KeyboardState _keyboardCurrent;
        private KeyboardState _keyboardPrevious;
        private Keys[] _keysPressed;

        private GamePadState _gamepadCurrent;
        private GamePadState _gamepadPrevious;
        private Buttons[] _buttonsPressed;

        //private MouseState _mouseCurrent;
        //private MouseState _mousePrevious;



        // Eventhandlers
        public event EventHandler<KeyboardChangedEventArgs> KeyChanged;
        public event EventHandler<KeyboardPressedEventArgs> KeyPressed;
        public event EventHandler<KeyboardReleasedEventArgs> KeyReleased;
        public event EventHandler<KeyboardDownEventArgs> KeyDown;

        public event EventHandler<GamepadChangedEventArgs> GamepadChanged;
        public event EventHandler<GamepadPressedEventArgs> GamepadPressed;
        public event EventHandler<GamepadDownEventArgs> GamepadDown;
        public event EventHandler<GamepadReleasedEventArgs> GamepadReleased;
        
        //public event EventHandler MouseMoved;
        //public event EventHandler MouseButtonChanged;
        //public event EventHandler MouseButtonPressed;
        //public event EventHandler MouseButtonDown;
        //public event EventHandler MouseButtonReleased;




        // Events
        private void OnKeyChange() { KeyChanged?.Invoke(this, new KeyboardChangedEventArgs(_keyboardPrevious, _keyboardCurrent)); }
        private void OnKeyPress() { KeyPressed?.Invoke(this, new KeyboardPressedEventArgs(_keyboardPrevious, _keyboardCurrent)); }
        private void OnKeyDown() { KeyDown?.Invoke(this, new KeyboardDownEventArgs(_keyboardPrevious, _keyboardCurrent)); }
        private void OnKeyRelease() { KeyReleased?.Invoke(this, new KeyboardReleasedEventArgs(_keyboardPrevious, _keyboardCurrent)); }

        private void OnGamepadChange() { GamepadChanged?.Invoke(this, new GamepadChangedEventArgs(_gamepadPrevious, _gamepadCurrent)); }
        private void OnGamepadPress() { GamepadPressed?.Invoke(this, new GamepadPressedEventArgs(_gamepadPrevious, _gamepadCurrent)); }
        private void OnGamepadDown() { GamepadDown?.Invoke(this, new GamepadDownEventArgs(_gamepadPrevious, _gamepadCurrent)); }
        private void OnGamepadRelease() { GamepadReleased?.Invoke(this, new GamepadReleasedEventArgs(_gamepadPrevious, _gamepadCurrent)); }

        //private void OnMouseMove() { MouseMoved?.Invoke(this, EventArgs.Empty); }
        //private void OnMouseButtonChange() { MouseButtonChanged?.Invoke(this, EventArgs.Empty); }
        //private void OnMouseButtonPress() { MouseButtonPressed?.Invoke(this, EventArgs.Empty); }
        //private void OnMouseButtonDown() { MouseButtonDown?.Invoke(this, EventArgs.Empty); }
        //private void OnMouseButtonRelease() { MouseButtonReleased?.Invoke(this, EventArgs.Empty); }

        // Functions


        public override void Update(GameTime gameTime)
        {
            UpdateKeyboard();
            UpdateGamepad();
            //UpdateMouse();

            base.Update(gameTime);
        }


        private void UpdateMouse()
        {
            throw new NotImplementedException();
        }

        private void UpdateGamepad()
        {
            _gamepadPrevious = _gamepadCurrent;
            _gamepadCurrent = GamePad.GetState(PlayerIndex.One);
            _buttonsPressed = _gamepadCurrent.GetPressedButtons();

            if (_buttonsPressed.Count() > 0) { OnGamepadDown(); }
            if (_gamepadCurrent != _gamepadPrevious)
            {
                OnGamepadChange();
                if (_buttonsPressed.Except(_gamepadPrevious.GetPressedButtons()).Count() > 0) { OnGamepadPress(); }
                if (_gamepadPrevious.GetPressedButtons().Except(_buttonsPressed).Count() > 0) { OnGamepadRelease(); }
            }
        }

        private void UpdateKeyboard()
        {
            _keyboardPrevious = _keyboardCurrent;
            _keyboardCurrent = Keyboard.GetState();
            _keysPressed = _keyboardCurrent.GetPressedKeys();

            if (_keysPressed.Count() > 0) { OnKeyDown(); }
            if (_keyboardCurrent != _keyboardPrevious)
            {
                OnKeyChange();
                if (_keysPressed.Except(_keyboardPrevious.GetPressedKeys()).Count() > 0) { OnKeyPress(); }
                if (_keyboardPrevious.GetPressedKeys().Except(_keysPressed).Count() > 0) { OnKeyRelease(); }
            }
        }

        public List<Keys> GetPressedKeys()
        {
            return _keyboardCurrent.GetPressedKeys().ToList();
        }
    }

    public class KeyboardReleasedEventArgs
    {
        public List<Keys> KeysReleased;
        public List<Keys> KeysDown;

        public KeyboardReleasedEventArgs(KeyboardState keyboardPrevious, KeyboardState keyboardCurrent)
        {
            var pressedPrevious = keyboardPrevious.GetPressedKeys();
            var pressedCurrent = keyboardCurrent.GetPressedKeys();
            var released = pressedPrevious.Except(pressedCurrent);

            KeysDown = pressedCurrent.ToList();
            KeysReleased = released.ToList();
        }
    }
    public class KeyboardPressedEventArgs
    {
        public List<Keys> Keys;

        public KeyboardPressedEventArgs(KeyboardState keyboardPrevious, KeyboardState keyboardCurrent)
        {
            var pressedPrevious = keyboardPrevious.GetPressedKeys();
            var pressedCurrent = keyboardCurrent.GetPressedKeys();
            var pressed = pressedCurrent.Except(pressedPrevious);

            Keys = pressed.ToList();
        }
    }
    public class KeyboardChangedEventArgs : EventArgs
    {
        public List<Keys> Keys;

        public KeyboardChangedEventArgs(KeyboardState keyboardPrevious, KeyboardState keyboardCurrent)
        {
            var pressedPrevious = keyboardPrevious.GetPressedKeys();
            var pressedCurrent = keyboardCurrent.GetPressedKeys();
            var changed = pressedPrevious.Except(pressedCurrent).Union(pressedCurrent.Except(pressedPrevious));

            Keys = changed.ToList();
        }
    }
    public class KeyboardDownEventArgs
    {
        public List<Keys> Keys;

        public KeyboardDownEventArgs(KeyboardState keyboardPrevious, KeyboardState keyboardCurrent)
        {
            var pressedCurrent = keyboardCurrent.GetPressedKeys();
            
            Keys = pressedCurrent.ToList();
        }
    }

    public class GamepadReleasedEventArgs
    {
        public List<Buttons> Buttons;

        public GamepadReleasedEventArgs(GamePadState gamepadPrevious, GamePadState gamepadCurrent)
        {
            var pressedPrevious = gamepadPrevious.GetPressedButtons();
            var pressedCurrent = gamepadCurrent.GetPressedButtons();
            var released = pressedPrevious.Except(pressedCurrent);

            Buttons = released.ToList();
        }
    }
    public class GamepadPressedEventArgs
    {
        public List<Buttons> Buttons;

        public GamepadPressedEventArgs(GamePadState gamepadPrevious, GamePadState gamepadCurrent)
        {
            var pressedPrevious = gamepadPrevious.GetPressedButtons();
            var pressedCurrent = gamepadCurrent.GetPressedButtons();
            var pressed = pressedCurrent.Except(pressedPrevious);

            Buttons = pressed.ToList();
        }
    }
    public class GamepadChangedEventArgs : EventArgs
    {
        public List<Buttons> Buttons;

        public GamepadChangedEventArgs(GamePadState gamepadPrevious, GamePadState gamepadCurrent)
        {
            var pressedPrevious = gamepadPrevious.GetPressedButtons();
            var pressedCurrent = gamepadCurrent.GetPressedButtons();
            var changed = pressedPrevious.Except(pressedCurrent).Union(pressedCurrent.Except(pressedPrevious));

            Buttons = changed.ToList();
        }
    }
    public class GamepadDownEventArgs
    {
        public List<Buttons> Buttons;

        public GamepadDownEventArgs(GamePadState gamepadPrevious, GamePadState gamepadCurrent)
        {
            var pressedCurrent = gamepadCurrent.GetPressedButtons();

            Buttons = pressedCurrent.ToList();
        }
    }

    /// <summary>
    /// Fun C# power- extension method for gamepadstates, to extend the original class to list all
    /// pressed buttons.
    /// 
    /// Example: https://msdn.microsoft.com/en-us/library/bb383977.aspx?f=255&MSPPError=-2147217396
    /// </summary>
    public static class GamePadStateExtensions
    {
        public static Buttons[] GetPressedButtons(this GamePadState gamePadState)
        {
            return Enum.GetValues(typeof(Buttons))
                       .Cast<Buttons>()
                       .Where(b => gamePadState.IsButtonDown(b))
                       .ToArray();
        }

    }
}

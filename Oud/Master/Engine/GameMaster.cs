﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System;
using Master.Gameobjects;
using System.Collections.Generic;
using Master.Engine;

namespace Master
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMaster : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private InputController _inputController;
        private TextController _textController;
        private ResourceController _resourceController;
        private Texture2D _boundingBoxTexture;

        public InputController InputController
        {
            get { return _inputController; }
        }
        public ResourceController ResourceController
        {
            get { return _resourceController; }
        }
        public TextController TextController { get { return _textController; } }
        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
        }
        public Size Size;

        public Texture2D BoundingBoxTexture { get { return _boundingBoxTexture; } }


        public GameMaster()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _graphics.PreferredBackBufferHeight = 600;
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.ApplyChanges();

            Size = new Size(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _inputController = InputController.Initialize(this);
            _resourceController = ResourceController.Initialize(this);
            _textController = TextController.Initialize(this);

            _boundingBoxTexture = new Texture2D(GraphicsDevice, 1, 1);
            _boundingBoxTexture.SetData(new[] { Color.White });

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), _spriteBatch); // is this really needed?
            _textController.SpriteBatch = _spriteBatch;


            // Purely for testing - should be removed from GameMaster eventually.
            var bg = new Background(this);
            var chickenManager = new ChickenManager(this);
            var treeManager = new TreeManager(this);
            var player = new Smurf(this, new Vector2(0, 520));

            bg.Player = player;
            chickenManager.Position = player.Position;
            chickenManager.Player = player;
            treeManager.Player = player;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            RemoveAllInstances();
        }

        private void RemoveAllInstances()
        {
            for (int i = Components.Count - 1; i >= 0; i--)
            {
                Components.RemoveAt(i);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) { Exit(); }

            base.Update(gameTime);
            CheckCollisions(GetAllInstances());

        }

        /// <summary>
        /// Get all gameobjects from the componentslist.
        /// 
        /// TODO: Review if this is needed, I think we only add gameobjects anyways.
        /// </summary>
        /// <returns></returns>
        public List<GameObject> GetAllInstances()
        {
            var instances = new List<GameObject>();
            var componentEnum = Components.ToList().GetEnumerator(); // tolist needed to fix a bug.

            while (componentEnum.MoveNext())
            {
                if (componentEnum.Current is GameObject) { instances.Add((GameObject)componentEnum.Current); }
            }

            return instances;
        }



        private void CheckCollisions(List<GameObject> instances)
        {
            instances.ForEach(x => x.UpdateBoundingBox()); // first make sure all boundingboxes are correct...
            instances.ForEach(x => x.CheckCollisions()); // ...then check collisions with other noteworthy instances.
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice?.Clear(Color.Black); // it's fun to remove this :)

            // TODO: Add your drawing code here
            _spriteBatch.Begin();
            base.Draw(gameTime); // moet tussen begin en eind staan!
            _spriteBatch.End();
        }

        /// <summary>
        /// Remove an instance from the game. Gets called when an instance gets destroyed.
        /// </summary>
        /// <param name="instance"></param>
        public void RemoveInstance(GameComponent instance)
        {
            Components?.Remove(instance);
        }

        /// <summary>
        /// Add an instance to the game. Gets called when an instance gets created.
        /// </summary>
        /// <param name="instance"></param>
        public void AddInstance(GameComponent instance)
        {
            Components?.Add(instance);
        }

        /// <summary>
        /// Utility. Get all instances that have a type that exists in the list of types we want to check.
        /// </summary>
        /// <param name="checkCollisionsWith"></param>
        /// <returns></returns>
        public List<GameObject> GetInstancesOfTypes(List<Type> checkCollisionsWith)
        {
            return (from x in GetAllInstances() where checkCollisionsWith.Contains(x.GetType()) select x).ToList();
        }
    }

    public class Size
    {
        private int height;
        private int width;

        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public int Height
        {
            get { return height; }
            set { height = (value >= 0 ? value : 0); }
        }

        public int Width
        {
            get { return width; }
            set { width = (value >= 0 ? value : 0); }
        }
    }
}

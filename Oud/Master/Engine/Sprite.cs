﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    public class Sprite
    {
        public int ID; // The ID of the sprite (currently unused)
        public SpriteEffects SpriteEffects; // Flip the sprite in a direction?
        public Texture2D Texture; // The texture containing the image
        public Vector2 Origin; // The point from where the image should draw
        public Vector2 Scale; // the scale of the sprite in both directions
        public Vector2 Size; // the size of the sprite
        public Rectangle BoundingBox; // The borders of the sprite
        public Color Color; // the overlaycolor of the sprite
        public float Rotation
        {
            get { return MathHelper.ToDegrees(_rotation); }
            set { _rotation = MathHelper.ToRadians(value); }
        } // the angle of rotation in degrees
        public Nullable<Rectangle> SourceRect; // the part of the sprite to use
        public List<Rectangle> Frames { get; set; } // All the frames we have at our disposal
        public bool Animate { get; set; }
        public int CurrentFrame
        {
            get { return _currentFrame; }
            set { _currentFrame = (Frames.Count == 0 ? 0 : value % Frames.Count); }
        } // The frame we're currently at, when cycling
        public int Index { get; set; } // The single frame we should show. When -1, we cycle through all frames
        public int Speed
        {
            get { return _speed; }
            set { _speed = (value < 0 ? 0 : value); _currentFrame = _speed; }
        }// The amount of frames before we continue to the next frame (barrel)
        public bool DrawBoundingBox
        {
            get { return _drawBoundingBox; }
            set { _drawBoundingBox = value; }
        }

        private int _speed;
        private float _rotation; // the angle of rotation in radians- damn you radians!!!
        private int _currentFrame; // ensures that we can never have a higher framecount than the amount of frames
        private int _frameIncreaseCounter; // Each time this reaches 0, we increase the current frame. Gets increased in Update().
        private bool _drawBoundingBox; // Draw a red rectangle behind the sprite? Only used for testpurposes.

        /// <summary>
        /// Constructor.
        /// </summary>
        public Sprite()
        {
            SpriteEffects = SpriteEffects.None;
            Texture = null;
            Origin = new Vector2(0, 0);
            Scale = new Vector2(100, 100);
            Size = new Vector2(0, 0);
            Rotation = 0;
            Color = Color.White;
            BoundingBox = new Rectangle(0, 0, 0, 0);
            SourceRect = null;
            Animate = false;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="texture">A texture for the sprite.</param>
        public Sprite(Texture2D texture, int nCols = 1, int nRows = 1, int speed = 1)
        {
            Set(texture, nCols, nRows, speed);
        }

        /// <summary>
        /// Default settings for MOST sprites.
        /// </summary>
        /// <param name="texture">A texture for the sprite.</param>
        /// <param name="nCols">The amount of horizontal frames on the sheet (columns)</param>
        /// <param name="nRows">The amount of vertical frames on the sheet (rows)</param>
        public void Set(Texture2D texture, int nCols = 1, int nRows = 1, int speed = 1)
        {
            Texture = texture;

            Size = (texture == null ? new Vector2(0, 0) : new Vector2(texture.Width, texture.Height));
            Origin = (texture == null ? new Vector2(0, 0) : new Vector2(texture.Width / 2, texture.Height / 2));
            BoundingBox = (texture == null ? new Rectangle(0, 0, 0, 0) : new Rectangle(0, 0, texture.Width, texture.Height));

            SpriteEffects = SpriteEffects.None;
            Scale = new Vector2(1, 1);
            Rotation = 0;
            Color = Color.White;
            SourceRect = null;
            Speed = speed;

            SplitIntoFrames(nCols, nRows);
        }

        /// <summary>
        /// Splits a texture into [y*x] frames, where the top-left frame is index 0, then each frame next to it is added by 1 
        /// until we go to the next row:
        /// 
        /// 0 1 2
        /// 3 4 5
        /// 
        /// All frames are added to Frames, in that order.
        /// </summary>
        /// <param name="nCols">The amount of horizontal frames on the sheet (columns)</param>
        /// <param name="nRows">The amount of vertical frames on the sheet (rows)</param>
        /// <param name="gapX">The horizontal distance between frames</param>
        /// <param name="gapY">The vertical distance between frames</param>
        private void SplitIntoFrames(int nCols, int nRows, int gapX = 0, int gapY = 0)
        {
            int frameWidth = (int)(Size.X / nCols);
            int frameHeight = (int)(Size.Y / nRows);

            Size = new Vector2(frameWidth, frameHeight);
            Origin = new Vector2(frameWidth / 2, frameHeight / 2);
            BoundingBox = new Rectangle(0, 0, frameWidth, frameHeight);

            Frames = new List<Rectangle>();
            CurrentFrame = 0;
            Animate = !(nCols == 1 && nRows == 1);

            _frameIncreaseCounter = 1;

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nCols; j++)
                {
                    Frames.Add(new Rectangle(j * (frameWidth + gapX), i * (frameHeight + gapY), frameWidth, frameHeight));
                }
            }
        }

        /// <summary>
        /// Draw the sprite.
        /// 
        /// TODO: remove spritebatch, and make it a field?
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="Position"></param>
        public void Draw(SpriteBatch spriteBatch, Vector2 Position)
        {
            if (Texture != null) { spriteBatch?.Draw(Texture, Position, Frames[CurrentFrame], Color, _rotation, Origin, Scale, SpriteEffects, 0); }
        }

        public void Update()
        {
            if (Animate)
            {
                if (_frameIncreaseCounter > 0) { _frameIncreaseCounter--; }
                if (_frameIncreaseCounter == 0)
                {
                    _frameIncreaseCounter = Speed;
                    _currentFrame = (_currentFrame + 1) % Frames.Count;
                }
            }


        }
    }
}
